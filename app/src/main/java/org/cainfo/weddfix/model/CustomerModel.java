package org.cainfo.weddfix.model;

/**
 * Created by Acer on 30-11-2016.
 */
public class CustomerModel {

    String name, feedback, date, rating;

    public CustomerModel(String name, String feedback, String date, String rating) {

        this.name = name;
        this.feedback = feedback;
        this.date = date;
        this.rating = rating;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getFeedback() {
        return feedback;
    }

    public void setFeedback(String feedback) {
        this.feedback = feedback;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }
}
