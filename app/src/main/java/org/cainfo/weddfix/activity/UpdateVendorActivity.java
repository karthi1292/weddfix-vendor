package org.cainfo.weddfix.activity;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import org.cainfo.weddfix.R;

public class UpdateVendorActivity extends AppCompatActivity implements View.OnClickListener {
    Toolbar toolbar;
    private LinearLayout btnProfile, btnBusiness, btnAbout, btnBank, btnGallery, btnPackage, btnReview, btnIdentity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_vendor);
        //To support different orientations for mobile and tablet
        if (getApplicationContext().getResources().getBoolean(R.bool.is_tablet)) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        } else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
        toolbar = (Toolbar) findViewById(R.id.appbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        btnProfile = (LinearLayout) findViewById(R.id.linear_profile_update);
        btnBusiness = (LinearLayout) findViewById(R.id.linear_business_profile_update);
        btnAbout = (LinearLayout) findViewById(R.id.linear_bank_update);
        btnBank = (LinearLayout) findViewById(R.id.linear_about_update);
        btnGallery = (LinearLayout) findViewById(R.id.linear_photos_update);
        btnPackage = (LinearLayout) findViewById(R.id.linear_package_update);
        btnReview = (LinearLayout) findViewById(R.id.linear_review_update);
        btnIdentity = (LinearLayout) findViewById(R.id.linear_identity_update);

        btnProfile.setOnClickListener(this);
        btnBusiness.setOnClickListener(this);
        btnAbout.setOnClickListener(this);
        btnBank.setOnClickListener(this);
        btnGallery.setOnClickListener(this);
        btnPackage.setOnClickListener(this);
        btnReview.setOnClickListener(this);
        btnIdentity.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.linear_profile_update:
                Intent profileIntent = new Intent(this, ProfileUpdateActivity.class);
                startActivity(profileIntent);
                break;
            case R.id.linear_business_profile_update:

                Intent businessIntent = new Intent(this, BusinessUpdateActivity.class);
                startActivity(businessIntent);
                break;
            case R.id.linear_about_update:
                Intent aboutIntent = new Intent(this, AboutBusinessUpdateActivity.class);
                startActivity(aboutIntent);
                break;
            case R.id.linear_bank_update:
                Intent bankIntent = new Intent(this, BankUpdateActivity.class);
                startActivity(bankIntent);
                break;
            case R.id.linear_photos_update:
                Intent galleryIntent = new Intent(this, GalleryUpdateActivity.class);
                startActivity(galleryIntent);
                break;
            case R.id.linear_package_update:
                Intent packageIntent = new Intent(this, OurPackageUpdateActivity.class);
                startActivity(packageIntent);
                break;
            case R.id.linear_review_update:
                break;
            case R.id.linear_identity_update:
                break;

        }
    }
}