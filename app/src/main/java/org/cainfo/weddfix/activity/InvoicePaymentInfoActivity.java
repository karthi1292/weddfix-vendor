package org.cainfo.weddfix.activity;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.pm.ActivityInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

import org.cainfo.weddfix.R;

import java.util.Calendar;

public class InvoicePaymentInfoActivity extends AppCompatActivity {
    EditText editDatePaid, editPayment;
    int day, month, year;
    CheckBox paidCheck;
    Calendar calendar;
    TextView txtPayment, txtDatePaid;
    private Toolbar toolbar;
    private DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {
            editDatePaid.setText(selectedYear + " / " + (selectedMonth + 1) + " / "
                    + selectedDay);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_info);
        //To support different orientations for mobile and tablet
        if(getApplicationContext().getResources().getBoolean(R.bool.is_tablet)){
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        }else{
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
        editDatePaid = (EditText) findViewById(R.id.input_invoice_date_paid);
        editPayment = (EditText) findViewById(R.id.input_invoice_partial_payment);
        paidCheck = (CheckBox) findViewById(R.id.checkBox);
        txtDatePaid = (TextView) findViewById(R.id.txt_invoice_date_paid);
        txtPayment = (TextView) findViewById(R.id.txt_invoice_partial_payment);
        toolbar = (Toolbar) findViewById(R.id.appbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }

        });

        paidCheck.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    //Highlight the text
                    txtDatePaid.setVisibility(View.VISIBLE);
                    editDatePaid.setVisibility(View.VISIBLE);
                    txtPayment.setVisibility(View.GONE);
                    editPayment.setVisibility(View.GONE);


                } else {
                    //Unhighlight the text
                    txtDatePaid.setVisibility(View.VISIBLE);
                    editPayment.setVisibility(View.VISIBLE);
                    editDatePaid.setVisibility(View.VISIBLE);
                    txtPayment.setVisibility(View.VISIBLE);
                }
            }
        });

        calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);
        editDatePaid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDialog(0);
            }
        });


    }

    @Override
    @Deprecated
    protected Dialog onCreateDialog(int id) {
        return new DatePickerDialog(this, datePickerListener, year, month, day);
    }
}
