package org.cainfo.weddfix.activity;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.ActivityInfo;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.timessquare.CalendarCellDecorator;
import com.squareup.timessquare.CalendarPickerView;
import com.squareup.timessquare.DefaultDayViewAdapter;

import org.cainfo.weddfix.R;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class AddCustomerActivity extends AppCompatActivity {
    String name, emails, address, city, state, zipcode;
    EditText editText1, editText2, editText3, editText4, editText5, editText6, editDay, editDescribtion;
    Button insert, btnAdd;
    private Context context;
    private Properties p;
    InputStream is = null;
    String result = null;
    String line = null;
    int code;
    public static String output = null;
    String emailRegEx;
    Pattern pattern;
    String email, token;
    LinearLayout container;
    Toolbar toolbar;
    private CalendarPickerView calendar;
    String pid;

    @SuppressLint("NewApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_form);
        //To support different orientations for mobile and tablet
        if(getApplicationContext().getResources().getBoolean(R.bool.is_tablet)){
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        }else{
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
        toolbar = (Toolbar) findViewById(R.id.appbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();
            }
        });

        final Calendar nextYear = Calendar.getInstance();
        nextYear.add(Calendar.YEAR, 1);

        final Calendar lastYear = Calendar.getInstance();
        lastYear.add(Calendar.YEAR, -1);

        calendar = (CalendarPickerView) findViewById(R.id.calendar_view);
        calendar.init(lastYear.getTime(), nextYear.getTime()) //
                .inMode(CalendarPickerView.SelectionMode.SINGLE) //
                .withSelectedDate(new Date());
        initButtonListeners(nextYear, lastYear);
        editText1 = (EditText) findViewById(R.id.et_name);
        editText2 = (EditText) findViewById(R.id.et_email);
        editText3 = (EditText) findViewById(R.id.et_address);
        editText4 = (EditText) findViewById(R.id.et_city);
        editText5 = (EditText) findViewById(R.id.et_state);
        editText6 = (EditText) findViewById(R.id.et_postalCode);
        editDay = (EditText) findViewById(R.id.et_day);
        editDescribtion = (EditText) findViewById(R.id.et_description);
        btnAdd = (Button) findViewById(R.id.add);
        container = (LinearLayout) findViewById(R.id.container);
        // Regex for a valid email address
        emailRegEx = "^[A-Za-z0-9._%+\\-]+@[A-Za-z0-9.\\-]+\\.[A-Za-z]{2,4}$";

        // onClick of button perform this simplest code.

        insert = (Button) findViewById(R.id.btn_save);
        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LayoutInflater layoutInflater =
                        (LayoutInflater) getBaseContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                final View addView = layoutInflater.inflate(R.layout.row_details, null);
                TextView textOut = (TextView) addView.findViewById(R.id.textout);
                TextView textOut2 = (TextView) addView.findViewById(R.id.text2out);
                textOut.setText(editDay.getText().toString());
                textOut2.setText(editDescribtion.getText().toString());
                Button buttonRemove = (Button) addView.findViewById(R.id.remove);
                buttonRemove.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        ((LinearLayout) addView.getParent()).removeView(addView);
                    }
                });

                container.addView(addView);
            }
        });


        insert.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                if (conMgr.getActiveNetworkInfo() != null && conMgr.getActiveNetworkInfo().isAvailable() && conMgr.getActiveNetworkInfo().isConnected()) {

                    name = editText1.getText().toString();
                    if (name.trim().equals("")) {
                        Toast.makeText(getBaseContext(), "Please Enter Name",
                                Toast.LENGTH_SHORT).show();
                        return;
                    }


                    emails = editText2.getText().toString();
                    if (emails.trim().equals("")) {
                        Toast.makeText(getBaseContext(), "Please Enter Email",
                                Toast.LENGTH_SHORT).show();
                        return;
                    }
                    pattern = Pattern.compile(emailRegEx);
                    Matcher matcher = pattern.matcher(emails);
                    if (!matcher.find()) {
                        Toast.makeText(getBaseContext(), "Please Enter Valid Email",
                                Toast.LENGTH_SHORT).show();
                        return;
                    }
                    address = editText3.getText().toString();
                    if (address.trim().equals("")) {
                        Toast.makeText(getBaseContext(), "Please Enter address",
                                Toast.LENGTH_SHORT).show();
                        return;
                    }
                    city = editText4.getText().toString();
                    if (city.trim().equals("")) {
                        Toast.makeText(getBaseContext(), "Please Enter city",
                                Toast.LENGTH_SHORT).show();
                        return;
                    }
                    state = editText5.getText().toString();
                    if (state.trim().equals("")) {
                        Toast.makeText(getBaseContext(), "Please Enter state",
                                Toast.LENGTH_SHORT).show();
                        return;
                    }
                    zipcode = editText6.getText().toString();
                    if (zipcode.trim().equals("")) {
                        Toast.makeText(getBaseContext(), "Please Enter Zipcode",
                                Toast.LENGTH_SHORT).show();
                        return;
                    }

                    //newuser();


                } else {
                    showAlertDialog(AddCustomerActivity.this, "No Internet Connection",
                            "Your device doesn't have mobile internet", false);
                    Toast.makeText(getBaseContext(), "No Internet Connection",
                            Toast.LENGTH_SHORT).show();
                }

            }

        });


    }


    private void initButtonListeners(final Calendar nextYear, final Calendar lastYear) {


        calendar.setCustomDayView(new DefaultDayViewAdapter());
        Calendar today = Calendar.getInstance();
        ArrayList<Date> dates = new ArrayList<Date>();
        for (int i = 0; i < 5; i++) {
            today.add(Calendar.DAY_OF_MONTH, 3);
            dates.add(today.getTime());
        }
        calendar.setDecorators(Collections.<CalendarCellDecorator>emptyList());
        calendar.init(new Date(), nextYear.getTime()) //
                .inMode(CalendarPickerView.SelectionMode.MULTIPLE) //
                .withSelectedDates(dates);

    }

    @SuppressWarnings("deprecation")
    public void showAlertDialog(Context context, String title, String message, Boolean status) {
        AlertDialog alertDialog = new AlertDialog.Builder(context).create();
        alertDialog.setTitle(title);
        alertDialog.setMessage(message);

        alertDialog.setButton2("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        alertDialog.show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                //startActivity(new Intent(context, Tab.class));
                // Intent intentreg = new Intent(getBaseContext(), MainActivity.class);
                // startActivity(intentreg);
                finish();


                //finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


}