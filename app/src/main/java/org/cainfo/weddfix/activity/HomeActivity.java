package org.cainfo.weddfix.activity;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;

import com.facebook.login.LoginManager;

import org.cainfo.weddfix.R;
import org.cainfo.weddfix.fragment.HomeFragment;
import org.cainfo.weddfix.fragment.ProfileFragment;
import org.cainfo.weddfix.fragment.ProfilePassChangeFragment;

public class HomeActivity extends AppCompatActivity {
    Toolbar toolbar;
    Fragment fragment;
    AppBarLayout appBarLayout;
    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;
    private NavigationView mNavigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        //To support different orientations for mobile and tablet
        if (getApplicationContext().getResources().getBoolean(R.bool.is_tablet)) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        } else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
        toolbar = (Toolbar) findViewById(R.id.appbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        initNavigationView();

        fragment = new HomeFragment();
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.ll_replace_fragment, fragment).commit();
        appBarLayout = (AppBarLayout) findViewById(R.id.app_bar_layout);

    }

    private void initNavigationView() {

        mNavigationView = (NavigationView) findViewById(R.id.navigation_drawer);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, toolbar, R.string.open_nav_drawer, R.string.close_nav_drawer) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                invalidateOptionsMenu();
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                invalidateOptionsMenu();
            }
        };
        mDrawerLayout.post(new Runnable() {
            @Override
            public void run() {
                mDrawerToggle.syncState();
            }
        });
        mDrawerToggle.setDrawerIndicatorEnabled(true);
        mDrawerLayout.setDrawerListener(mDrawerToggle);
        MenuItem menu = mNavigationView.getMenu().findItem(R.id.menu_logout);


        mNavigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {

                mDrawerLayout.closeDrawers();

                switch (menuItem.getItemId()) {


                    case R.id.menu_home:

                        fragment = new HomeFragment();
                        FragmentManager fragmentManager = getSupportFragmentManager();
                        fragmentManager.beginTransaction().replace(R.id.ll_replace_fragment, fragment).commit();
                        break;


                    case R.id.menu_logout:
                        LoginManager.getInstance().logOut();
                        startActivity(new Intent(HomeActivity.this, LoginActivity.class));
                        finish();
                        break;

                    case R.id.menu_myAccount:
                        fragment = new ProfileFragment();
                        FragmentManager profileManager = getSupportFragmentManager();
                        profileManager.beginTransaction().replace(R.id.ll_replace_fragment, fragment).commit();
                        break;


                    case R.id.menu_setting:
                        fragment = new ProfilePassChangeFragment();
                        FragmentManager settingManager = getSupportFragmentManager();
                        settingManager.beginTransaction().replace(R.id.ll_replace_fragment, fragment).commit();
                        break;


                    case R.id.menu_support:
                        break;
                    case R.id.menu_customer:
                        Intent intentCustomer = new Intent(HomeActivity.this, CustomerActivity.class);
                        startActivity(intentCustomer);

                        break;

                    case R.id.menu_invoice:
                        Intent intent = new Intent(HomeActivity.this, AddInvoiceActivity.class);
                        startActivity(intent);

                        break;
                    case R.id.menu_myBooking:

                        break;

                }


                return false;
            }

        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        return true;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        mDrawerToggle.onOptionsItemSelected(item);
        switch (item.getItemId()) {

            case R.id.menU_actionbar_search:


                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();


    }

}
