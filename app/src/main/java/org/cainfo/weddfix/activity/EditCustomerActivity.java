package org.cainfo.weddfix.activity;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


import com.squareup.timessquare.CalendarCellDecorator;
import com.squareup.timessquare.CalendarPickerView;
import com.squareup.timessquare.DefaultDayViewAdapter;

import org.cainfo.weddfix.R;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Properties;
import java.util.Random;
import java.util.regex.Pattern;



public class EditCustomerActivity extends AppCompatActivity {

    String name, emails, address, city, state, zipcode;
    EditText editText1, editText2, editText3, editText4, editText5, editText6;
    Button insert, login;
    private Context context;

    private Properties p;
    InputStream is = null;
    String result = null;
    String line = null;
    int code;
    public static String output = null;
    String emailRegEx;
    Pattern pattern;
    String email, token;
    Toolbar toolbar;
    String cli_pid, cli_name, cli_email, cli_city, cli_address, cli_state, cli_zip;
    private CalendarPickerView calendar;
    @SuppressLint("NewApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_form);
        //To support different orientations for mobile and tablet
        if(getApplicationContext().getResources().getBoolean(R.bool.is_tablet)){
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        }else{
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
        toolbar = (Toolbar) findViewById(R.id.appbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();
            }
        });
        final Calendar nextYear = Calendar.getInstance();
        nextYear.add(Calendar.YEAR, 1);

        final Calendar lastYear = Calendar.getInstance();
        lastYear.add(Calendar.YEAR, -1);

        calendar = (CalendarPickerView) findViewById(R.id.calendar_view);
        calendar.init(lastYear.getTime(), nextYear.getTime()) //
                .inMode(CalendarPickerView.SelectionMode.SINGLE) //
                .withSelectedDate(new Date());
        initButtonListeners(nextYear, lastYear);

        editText1 = (EditText) findViewById(R.id.et_name);
        editText2 = (EditText) findViewById(R.id.et_email);
        editText3 = (EditText) findViewById(R.id.et_address);
        editText4 = (EditText) findViewById(R.id.et_city);
        editText5 = (EditText) findViewById(R.id.et_state);
        editText6 = (EditText) findViewById(R.id.et_postalCode);
        Intent i = getIntent();
       /* cli_pid = i.getStringExtra("id");
        cli_name = i.getStringExtra("name");
        cli_email = i.getStringExtra("email");
        cli_address = i.getStringExtra("address");
        cli_city = i.getStringExtra("city");
        cli_state = i.getStringExtra("state");
        cli_zip = i.getStringExtra("zip");
        Toast.makeText(getBaseContext(), cli_name,
                Toast.LENGTH_SHORT).show();*/


        // Regex for a valid email address
        emailRegEx = "^[A-Za-z0-9._%+\\-]+@[A-Za-z0-9.\\-]+\\.[A-Za-z]{2,4}$";

        // onClick of button perform this simplest code.

        insert = (Button) findViewById(R.id.btn_save);


        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }


        insert.setOnClickListener(new View.OnClickListener() {
                                      public void onClick(View v) {
                                          ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                                          if (conMgr.getActiveNetworkInfo() != null && conMgr.getActiveNetworkInfo().isAvailable() && conMgr.getActiveNetworkInfo().isConnected()) {

                                              name = editText1.getText().toString();
                                              //editText1.setText(cli_name);

                                              emails = editText2.getText().toString();
                                             // editText2.setText(cli_email);

                                              address = editText3.getText().toString();
                                              //editText3.setText(cli_address);

                                              city = editText4.getText().toString();
                                              //editText4.setText(cli_city);

                                              state = editText5.getText().toString();
                                              //editText5.setText(cli_state);

                                              zipcode = editText6.getText().toString();
                                              //editText6.setText(cli_zip);

                                              //newuser();


                                          } else

                                          {
                                              showAlertDialog(EditCustomerActivity.this, "No Internet Connection",
                                                      "Your device doesn't have mobile internet", false);
                                              Toast.makeText(getBaseContext(), "No Internet Connection",
                                                      Toast.LENGTH_SHORT).show();
                                          }

                                      }

                                  }

        );


    }

    private void initButtonListeners(final Calendar nextYear, final Calendar lastYear) {


        calendar.setCustomDayView(new DefaultDayViewAdapter());
        Calendar today = Calendar.getInstance();
        ArrayList<Date> dates = new ArrayList<Date>();
        for (int i = 0; i < 5; i++) {
            today.add(Calendar.DAY_OF_MONTH, 3);
            dates.add(today.getTime());
        }
        calendar.setDecorators(Collections.<CalendarCellDecorator>emptyList());
        calendar.init(new Date(), nextYear.getTime()) //
                .inMode(CalendarPickerView.SelectionMode.MULTIPLE) //
                .withSelectedDates(dates);

    }

    @SuppressWarnings("deprecation")
    public void showAlertDialog(Context context, String title, String message, Boolean status) {
        AlertDialog alertDialog = new AlertDialog.Builder(context).create();
        alertDialog.setTitle(title);
        alertDialog.setMessage(message);

        alertDialog.setButton2("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        alertDialog.show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                //startActivity(new Intent(context, Tab.class));
                // Intent intentreg = new Intent(getBaseContext(), MainActivity.class);
                // startActivity(intentreg);
                finish();


                //finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
