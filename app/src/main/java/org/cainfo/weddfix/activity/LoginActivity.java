package org.cainfo.weddfix.activity;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.cainfo.weddfix.Interfaces.DialogResponse;
import org.cainfo.weddfix.Interfaces.VendorTriggerWithString;
import org.cainfo.weddfix.R;
import org.cainfo.weddfix.common.VendorCommonMethods;
import org.cainfo.weddfix.common.VendorConstants;
import org.cainfo.weddfix.dialog.VendorForgotPasswordDialog;


public class LoginActivity extends AppCompatActivity implements View.OnClickListener {
    Button signUp, loginBtn;
    TextView forgotPassword, textUrlLink, textPhoneCall;
    Dialog alertDialogBuilder;
    final Context mContext = this;
    private EditText editEmail, editPassword;
    private VendorForgotPasswordDialog vendorForgotPasswordDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        loginBtn = (Button) findViewById(R.id.login_btn);
        signUp = (Button) findViewById(R.id.sign_up_btn);
        forgotPassword = (TextView) findViewById(R.id.click_here);
        textUrlLink = (TextView) findViewById(R.id.txt_url_link);
        textPhoneCall = (TextView) findViewById(R.id.txt_call_enquiry);
        editEmail = (EditText) findViewById(R.id.ed_login_email);
        editPassword = (EditText) findViewById(R.id.ed_login_password);

        loginBtn.setOnClickListener(this);
        forgotPassword.setOnClickListener(this);
        signUp.setOnClickListener(this);
        textUrlLink.setOnClickListener(this);
        textPhoneCall.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.login_btn:
                Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
                startActivity(intent);
                break;
            case R.id.sign_up_btn:
                Intent signUpIntent = new Intent(LoginActivity.this, RegistrationActivity.class);
                startActivity(signUpIntent);
                break;
            case R.id.click_here:
                vendorForgotPasswordDialog = new VendorForgotPasswordDialog(this, "", new VendorTriggerWithString() {
                    @Override
                    public void initTrigger(boolean Trigger, String forgotPwdEmailId) {
                        if (forgotPwdEmailId != null) {
                            if (VendorCommonMethods.isNetworkConnectionAvailable(mContext)) {
                                // sendForgotPassword(forgotPwdEmailId);
                            } else {
                                VendorCommonMethods.showToast(VendorConstants.TOAST_CONNECTION_ERROR, LoginActivity.this);
                            }
                        } else {
                            VendorCommonMethods.showToast("Please enter your Registered mailId", mContext);
                        }
                    }
                });
                vendorForgotPasswordDialog.showDialog();
                break;
            case R.id.txt_url_link:
                String url = "https://weddfix.com";
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
                break;
            case R.id.txt_call_enquiry:
                onCall();
                break;
        }
    }

    private void releseWindowToTouch() {
        //progressBarContainer.setVisibility(View.GONE);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    public void onCall() {
        int permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE);

        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(
                    this,
                    new String[]{Manifest.permission.CALL_PHONE},
                    123);
        } else {
            startActivity(new Intent(Intent.ACTION_CALL).setData(Uri.parse("tel:9965916911")));
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {

            case 123:
                if ((grantResults.length > 0) && (grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    onCall();
                } else {

                    Log.d("TAG", "Call Permission Not Granted");
                }
                break;

            default:
                break;
        }
    }
}
