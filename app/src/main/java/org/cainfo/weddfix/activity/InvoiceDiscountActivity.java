package org.cainfo.weddfix.activity;

import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import org.cainfo.weddfix.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class InvoiceDiscountActivity extends AppCompatActivity {
    Toolbar toolbar;
    TextView txtPercentage,txtAmount;
    EditText editPercentage,editAmount;
    Spinner spinner;
    String[] spinnerItems = new String[]{
            "Select Discount Option",
            "No Discount",
            "Discount per function",
            "Percentage","Flat Amount"

    };

    ArrayAdapter<String> arrayadapter;
    List<String> spinnerlist;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invoice_discount);
        //To support different orientations for mobile and tablet
        if(getApplicationContext().getResources().getBoolean(R.bool.is_tablet)){
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        }else{
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
        toolbar = (Toolbar) findViewById(R.id.appbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle("Discount");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        spinner = (Spinner) findViewById(R.id.spinner);
        txtPercentage= (TextView) findViewById(R.id.txt_discount_percentage);
        txtAmount= (TextView) findViewById(R.id.txt_flat_amount);
        editPercentage= (EditText) findViewById(R.id.input_invoice_discount_percentage);
        editAmount= (EditText) findViewById(R.id.input_discount_flatAmount);


        spinnerlist = new ArrayList<>(Arrays.asList(spinnerItems));
        arrayadapter = new ArrayAdapter<String>(InvoiceDiscountActivity.this, R.layout.custome_payment_text, spinnerlist) {
            @Override
            public boolean isEnabled(int position) {
                if (position == 0) {
                    //Disable the third item of spinner.

                    return false;
                } else {
                    return true;
                }
            }

            @Override
            public View getDropDownView(int position, View convertView, ViewGroup parent) {
                View spinnerview = super.getDropDownView(position, convertView, parent);

                TextView spinnertextview = (TextView) spinnerview;

                if (position == 0) {

                    //Set the disable spinner item color fade .
                    spinnertextview.setTextColor(Color.parseColor("#000000"));
                } else {

                    spinnertextview.setTextColor(Color.WHITE);
                    // spinnertextview.setGravity(Gravity.CENTER);

                }
                return spinnerview;
            }
        };

        arrayadapter.setDropDownViewResource(R.layout.custome_payment_text);

        spinner.setAdapter(arrayadapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selectedItemText = (String) parent.getItemAtPosition(position);
                // If user change the default selection
                // First item is disable and it is used for hint
                if (position== 0) {
                    txtPercentage.setVisibility(View.GONE);
                    txtAmount.setVisibility(View.GONE);
                    editAmount.setVisibility(View.GONE);
                    editPercentage.setVisibility(View.GONE);

                    // Toast.makeText(getApplicationContext(), "Selected : " + selectedItemText, Toast.LENGTH_SHORT).show();
                }else if(position==1){
                    txtPercentage.setVisibility(View.GONE);
                    txtAmount.setVisibility(View.GONE);
                    editAmount.setVisibility(View.GONE);
                    editPercentage.setVisibility(View.GONE);

                }else if(position==2){
                    txtPercentage.setVisibility(View.GONE);
                    txtAmount.setVisibility(View.GONE);
                    editAmount.setVisibility(View.GONE);
                    editPercentage.setVisibility(View.GONE);

                }else if(position==3){
                    txtPercentage.setVisibility(View.VISIBLE);
                    txtAmount.setVisibility(View.GONE);
                    editAmount.setVisibility(View.GONE);
                    editPercentage.setVisibility(View.VISIBLE);

                }else if (position==4){
                    txtPercentage.setVisibility(View.GONE);
                    txtAmount.setVisibility(View.VISIBLE);
                    editAmount.setVisibility(View.VISIBLE);
                    editPercentage.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


    }
}
