package org.cainfo.weddfix.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.cainfo.weddfix.R;
import org.cainfo.weddfix.adapter.AdapterCustomer;
import org.cainfo.weddfix.model.CustomerModel;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.prefs.Preferences;

public class CustomerActivity extends AppCompatActivity {

    ProgressDialog progressDialog;
    RecyclerView recyclerView;
    LinearLayoutManager layoutManager;
    ArrayList<CustomerModel> feedList;
    AdapterCustomer adapter_feedback;
    Bundle bundle;
    String[] Names = {"Ramesh", "Balaji", "Siva", "Arun", "Varun", "Guru", "Suresh", "Naveen", "Karthik"};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer);
        //To support different orientations for mobile and tablet
        if(getApplicationContext().getResources().getBoolean(R.bool.is_tablet)){
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        }else{
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
        progressDialog = new ProgressDialog(CustomerActivity.this);
        feedList = new ArrayList<>();
        Toolbar toolbar;
        toolbar = (Toolbar) findViewById(R.id.appbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        recyclerView = (RecyclerView) findViewById(R.id.recycler_clientlist);
        layoutManager = new LinearLayoutManager(CustomerActivity.this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);


        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Click action
                startActivity(new Intent(CustomerActivity.this, AddCustomerActivity.class));
            }
        });
        populatRecyclerView();
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                //startActivity(new Intent(context, Tab.class));
                Intent intentreg = new Intent(getBaseContext(), HomeActivity.class);
                startActivity(intentreg);
                finish();

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    private void populatRecyclerView() {


        for (int i = 0; i < Names.length; i++) {

            feedList.add(new CustomerModel(Names[i], Names[i], Names[i], Names[i]));
        }
        adapter_feedback = new AdapterCustomer(this, feedList);

        recyclerView.setAdapter(adapter_feedback);

        adapter_feedback.notifyDataSetChanged();// Notify the adapter
    }

}
