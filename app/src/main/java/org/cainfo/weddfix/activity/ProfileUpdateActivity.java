package org.cainfo.weddfix.activity;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import org.cainfo.weddfix.R;
import org.cainfo.weddfix.common.Config;
import org.cainfo.weddfix.common.Prefs;

import java.io.ByteArrayOutputStream;
import java.io.File;

import de.hdodenhof.circleimageview.CircleImageView;

public class ProfileUpdateActivity extends AppCompatActivity {

    public static final int MEDIA_TYPE_IMAGE = 1;
    private static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 100;
    Toolbar toolbar;
    CircleImageView profile_image;
    SharedPreferences sharedPreferences;
    EditText ed_name, ed_email, ed_pincode, ed_phone;
    String name, email, pinCode, phone;
    String filePath = null;
    String image;
    String url_dp_refresh;
    Uri fileUri;
    FloatingActionButton fab;
    ProgressDialog progressDialog;
    private Bitmap bitmap;
    private String KEY_IMAGE = "image";
    private String KEY_NAME = "name";
    private int PICK_IMAGE_REQUEST = 1;


    public static Bitmap decodeSampledBitmapFromResource(String path,
                                                         int reqWidth, int reqHeight) {

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeFile(path, options);
    }

    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_update);

        //To support different orientations for mobile and tablet
        if (getApplicationContext().getResources().getBoolean(R.bool.is_tablet)) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        } else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
        toolbar = (Toolbar) findViewById(R.id.appbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        profile_image = (CircleImageView) findViewById(R.id.img_profile_upload);

        fab = (FloatingActionButton) findViewById(R.id.fab_profile_edit_submit_update);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                name = ed_name.getText().toString();
                email = ed_email.getText().toString();
                phone = ed_phone.getText().toString();
                pinCode = ed_pincode.getText().toString();


                if (name.isEmpty()) {

                    Toast.makeText(ProfileUpdateActivity.this, "First Name should not be empty", Toast.LENGTH_SHORT).show();

                } else if (email.isEmpty()) {

                    Toast.makeText(ProfileUpdateActivity.this, "Email should not be empty", Toast.LENGTH_SHORT).show();
                } else if (phone.isEmpty()) {

                    Toast.makeText(ProfileUpdateActivity.this, "Mobile Number should not be empty", Toast.LENGTH_SHORT).show();
                } else if (pinCode.isEmpty()) {

                    Toast.makeText(ProfileUpdateActivity.this, "Pincode should not be empty", Toast.LENGTH_SHORT).show();
                } else {
                    name = name.replace(" ", "%20");
                    phone = phone.replace(" ", "%20");
                    progressDialog.setMessage("updating");
                    progressDialog.setCancelable(false);
                    progressDialog.show();
                }
            }
        });
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        profile_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startDialog();

            }
        });


        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(ProfileUpdateActivity.this);
        new Thread(new Runnable() {
            @Override
            public void run() {
                Glide.get(ProfileUpdateActivity.this).clearDiskCache();
            }
        }).start();

        ed_name = (EditText) findViewById(R.id.ed_profile_edit_name);
        ed_email = (EditText) findViewById(R.id.ed_profile_edit_email);
        ed_pincode = (EditText) findViewById(R.id.ed_userRegister_pinCode);
        ed_phone = (EditText) findViewById(R.id.ed_profile_edit_phone);
        ed_email.setEnabled(false);

    }


    private void startDialog() {

        AlertDialog.Builder myAlertDialog = new AlertDialog.Builder(
                ProfileUpdateActivity.this);
        myAlertDialog.setTitle("Upload Pictures Option");
        myAlertDialog.setMessage("How do you want to set your picture?");

        myAlertDialog.setPositiveButton("Gallery",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

                        startActivityForResult(intent, PICK_IMAGE_REQUEST);

                    }
                });

        myAlertDialog.setNegativeButton("Camera",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                        captureImage();

                    }
                });
        myAlertDialog.show();
    }

    private void captureImage() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);

        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);

        // start the image capture Intent
        startActivityForResult(intent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
    }


    public Uri getOutputMediaFileUri(int type) {

        return Uri.fromFile(getOutputMediaFile(type));
    }

    private File getOutputMediaFile(int type) {

        // External sdcard location
        File mediaStorageDir = new File(
                Environment.getExternalStorageDirectory().getAbsolutePath(),
                Config.IMAGE_DIRECTORY_NAME);

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d("ramesh", "Oops! Failed create "
                        + Config.IMAGE_DIRECTORY_NAME + " directory");
                return null;
            }
        }

        // Create a media file name
       /* String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                Locale.getDefault()).format(new Date());*/
        File mediaFile;
        if (type == MEDIA_TYPE_IMAGE) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "avatar_" + sharedPreferences.getString(Prefs.U_ID, "") + ".jpg");
        } else {
            return null;
        }

        return mediaFile;
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!Settings.System.canWrite(this)) {
                requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.READ_EXTERNAL_STORAGE}, 2909);
                if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null) {

                    Uri pickedImage = data.getData();
                    // Let's read picked image path using content resolver
                    String[] filePath = {MediaStore.Images.Media.DATA};
                    Cursor cursor = getContentResolver().query(pickedImage, filePath, null, null, null);
                    cursor.moveToFirst();
                    String imagePath = cursor.getString(cursor.getColumnIndex(filePath[0]));
                    fileUri = Uri.parse(imagePath);
                    Log.d("ramesh", fileUri + " gallery");
                    Log.d("ramesh", imagePath + " img_gallery");
                    bitmap = decodeSampledBitmapFromResource(imagePath, 100, 100);
                    profile_image.setImageBitmap(decodeSampledBitmapFromResource(imagePath, 100, 100));

                    // new Uploadtoserver().execute();
                    // Now we need to set the GUI ImageView data with data read from the picked file.
//            Glide.with(ProfileEditActivity.this).load(BitmapFactory.decodeFile(imagePath)).into(profile_image);


                    // At the end remember to close the cursor or you will end with the RuntimeException!
                    cursor.close();
                }
                if (requestCode == CAMERA_CAPTURE_IMAGE_REQUEST_CODE) {
                    if (resultCode == RESULT_OK) {

                        // successfully captured the image
                        // launching upload activity
                        Log.d("ramesh", fileUri.getPath() + " camera Image");
                        bitmap = decodeSampledBitmapFromResource(fileUri.getPath(), 100, 100);
                        // new Uploadtoserver().execute();

                        profile_image.setImageBitmap(bitmap);


                    } else if (resultCode == RESULT_CANCELED) {

                        // user cancelled Image capture
                        Toast.makeText(getApplicationContext(),
                                "User cancelled image capture", Toast.LENGTH_SHORT)
                                .show();

                    } else {
                        // failed to capture image
                        Toast.makeText(getApplicationContext(),
                                "Sorry! Failed to capture image", Toast.LENGTH_SHORT)
                                .show();
                    }

                }
            } else {

            }
        } else {

        }
    }


    public String getStringImage(Bitmap bmp) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] imageBytes = baos.toByteArray();
        String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        return encodedImage;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case 2909: {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.e("Permission", "Granted");
                } else {
                    Log.e("Permission", "Denied");
                }
                return;
            }
        }
    }
}
