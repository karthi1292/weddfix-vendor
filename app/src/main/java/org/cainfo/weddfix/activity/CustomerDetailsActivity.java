package org.cainfo.weddfix.activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.cainfo.weddfix.R;


public class CustomerDetailsActivity extends AppCompatActivity {

    TextView name, email, address, city, state, zip;
    Toolbar toolbar;
    Button editBtn,invoiceBtn;
    String cli_pid, cli_name, cli_email, cli_city, cli_address, cli_state, cli_zip;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_details);

        //To support different orientations for mobile and tablet
        if(getApplicationContext().getResources().getBoolean(R.bool.is_tablet)){
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        }else{
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
        toolbar = (Toolbar) findViewById(R.id.appbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();
            }
        });

        name = (TextView) findViewById(R.id.customer_name);
        email = (TextView) findViewById(R.id.client_email);
        address = (TextView) findViewById(R.id.client_address);
        city = (TextView) findViewById(R.id.client_city);
        state = (TextView) findViewById(R.id.client_state);
        zip = (TextView) findViewById(R.id.client_zip);
     /*   editBtn = (Button) findViewById(R.id.btn_edit);*/
        invoiceBtn= (Button) findViewById(R.id.invoice_btnId);
        invoiceBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(CustomerDetailsActivity.this,EditInvoiceActivity.class);
                startActivity(intent);
            }
        });
        /*editBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {



                i.putExtra("id", cli_pid);
                i.putExtra("name", cli_name);
                i.putExtra("email", cli_email);
                i.putExtra("city", cli_city);
                i.putExtra("address", cli_address);
                i.putExtra("state", cli_state);
                i.putExtra("zip", cli_zip);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);*//*
                startActivity(i);
            }
        });*/

       /* Intent i = getIntent();
        cli_pid = i.getStringExtra("id");
        cli_name = i.getStringExtra("name");
        cli_email = i.getStringExtra("email");
        cli_address = i.getStringExtra("address");
        cli_city = i.getStringExtra("city");
        cli_state = i.getStringExtra("state");
        cli_zip = i.getStringExtra("zip");*/

        /*ConnectivityManager cn = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo nf = cn.getActiveNetworkInfo();
        if (nf != null && nf.isConnected() == true) {

        name.setText(cli_name);
        email.setText(cli_email);
        address.setText(cli_address);
        city.setText(cli_city);
        state.setText(cli_state);
        zip.setText(cli_zip);

        } else {
            showAlertDialog(CustomerDetailsActivity.this, "No Internet Connection",
                    "Your device doesn't have mobile internet", false);
            Toast.makeText(getBaseContext(), "No Internet Connection",
                    Toast.LENGTH_SHORT).show();
        }*/
    }

    @SuppressWarnings("deprecation")
    public void showAlertDialog(Context context, String title, String message, Boolean status) {
        AlertDialog alertDialog = new AlertDialog.Builder(context).create();
        alertDialog.setTitle(title);
        alertDialog.setMessage(message);

        alertDialog.setButton2("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        alertDialog.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_edit, menu);
        return true;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.menu_actionbar_edit) {
            Intent i = new Intent(CustomerDetailsActivity.this,EditCustomerActivity.class);
            startActivity(i);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


}
