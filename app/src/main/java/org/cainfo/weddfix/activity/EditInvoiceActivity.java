package org.cainfo.weddfix.activity;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;

import org.cainfo.weddfix.R;
import org.cainfo.weddfix.fragment.InvoiceEditFragment;
import org.cainfo.weddfix.fragment.InvoiceHistoryFragment;
import org.cainfo.weddfix.fragment.InvoicePreviewFragment;


public class EditInvoiceActivity extends AppCompatActivity implements View.OnClickListener {
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private Button editBnt, pdfBtn, emailBtn, socialBtn, paymentBtn, deleteBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invoice);

        //To support different orientations for mobile and tablet
        if (getApplicationContext().getResources().getBoolean(R.bool.is_tablet)) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        } else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }

        Toolbar toolbar = (Toolbar) findViewById(R.id.appbar);
        setSupportActionBar(toolbar);
        toolbar.setTitleTextColor(Color.parseColor("#ffffff"));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                finish();
            }
        });

        viewPager = (ViewPager) findViewById(R.id.pager);
        editBnt = (Button) findViewById(R.id.edit_btn);
        pdfBtn = (Button) findViewById(R.id.pdf_btn);
        emailBtn = (Button) findViewById(R.id.email_btn);
        socialBtn = (Button) findViewById(R.id.public_btn);
        deleteBtn = (Button) findViewById(R.id.delete_btn);
        paymentBtn = (Button) findViewById(R.id.payment_btn);

        editBnt.setOnClickListener(this);
        pdfBtn.setOnClickListener(this);
        emailBtn.setOnClickListener(this);
        socialBtn.setOnClickListener(this);
        deleteBtn.setOnClickListener(this);
        paymentBtn.setOnClickListener(this);

        viewPager.setAdapter(new CustomAdapter(getSupportFragmentManager(), getApplicationContext()));

        tabLayout = (TabLayout) findViewById(R.id.tabs_product_tabs);
        tabLayout.setTabTextColors(Color.parseColor("#ffffff"), Color.parseColor("#ffffff"));
        tabLayout.setupWithViewPager(viewPager);

        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }
        });

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.edit_btn:
                Intent intentEdit = new Intent(this, EditInvoiceActivity.class);
                startActivity(intentEdit);
                finish();
                break;
            case R.id.pdf_btn:
                break;
            case R.id.email_btn:

                break;
            case R.id.payment_btn:

                break;
            case R.id.delete_btn:

                break;
            case R.id.public_btn:

                break;

        }

    }


    //RequiresPermission.Read more: http://www.androidhub4you.com/2012/07/how-to-create-popup-window-in-android.html#ixzz4K1JmX73i
    private class CustomAdapter extends FragmentPagerAdapter {

        private String fragments[] = {"Edit", "Preview", "History "};

        public CustomAdapter(FragmentManager supportFragmentManager, Context applicationContext) {
            super(supportFragmentManager);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return new InvoiceEditFragment();
                case 1:
                    return new InvoicePreviewFragment();
                case 2:
                    return new InvoiceHistoryFragment();
                default:
                    return null;
            }
        }

        @Override
        public int getCount() {
            return fragments.length;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return fragments[position];
        }
    }
}
