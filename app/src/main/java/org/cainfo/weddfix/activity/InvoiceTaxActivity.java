package org.cainfo.weddfix.activity;

import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import org.cainfo.weddfix.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class InvoiceTaxActivity extends AppCompatActivity {
    Toolbar toolbar;
    TextView txtLabel,txtRate,txtInclusive,txtCheck,txtUnCheck;
    EditText editLabel,editRate;
    CheckBox taxCheck;
    Spinner spinner;
    String[] spinnerItems = new String[]{
            "Select Tax Type",
            "On the Total",
            "Detected",
            "Per item","None"

    };

    ArrayAdapter<String> arrayadapter;
    List<String> spinnerlist;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invoice_tax);
        //To support different orientations for mobile and tablet
        if(getApplicationContext().getResources().getBoolean(R.bool.is_tablet)){
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        }else{
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
        toolbar = (Toolbar) findViewById(R.id.appbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle("Discount");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        spinner = (Spinner) findViewById(R.id.spinner);
        txtLabel= (TextView) findViewById(R.id.txt_tax_label);
        txtRate= (TextView) findViewById(R.id.txt_tax_rate);
        txtInclusive= (TextView) findViewById(R.id.txt_tax_inclusive);
        txtCheck= (TextView) findViewById(R.id.txt_inclusive_check);
        txtUnCheck= (TextView) findViewById(R.id.txt_inclusive_unCheck);
        editLabel= (EditText) findViewById(R.id.input_invoice_tax_label);
        editRate= (EditText) findViewById(R.id.input_tax_rate);
        taxCheck= (CheckBox) findViewById(R.id.checkBox);

        taxCheck.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if (isChecked)
                {
                    //Highlight the text
                    txtCheck.setVisibility(View.VISIBLE);
                    txtUnCheck.setVisibility(View.GONE);


                } else
                {
                    //Unhighlight the text
                    txtCheck.setVisibility(View.GONE);
                    txtUnCheck.setVisibility(View.VISIBLE);

                }
            }
        });





        spinnerlist = new ArrayList<>(Arrays.asList(spinnerItems));
        arrayadapter = new ArrayAdapter<String>(InvoiceTaxActivity.this, R.layout.custome_payment_text, spinnerlist) {
            @Override
            public boolean isEnabled(int position) {
                if (position == 0) {
                    //Disable the third item of spinner.

                    return false;
                } else {
                    return true;
                }
            }

            @Override
            public View getDropDownView(int position, View convertView, ViewGroup parent) {
                View spinnerview = super.getDropDownView(position, convertView, parent);

                TextView spinnertextview = (TextView) spinnerview;

                if (position == 0) {

                    //Set the disable spinner item color fade .
                    spinnertextview.setTextColor(Color.parseColor("#000000"));
                } else {

                    spinnertextview.setTextColor(Color.WHITE);
                    // spinnertextview.setGravity(Gravity.CENTER);

                }
                return spinnerview;
            }
        };

        arrayadapter.setDropDownViewResource(R.layout.custome_payment_text);

        spinner.setAdapter(arrayadapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selectedItemText = (String) parent.getItemAtPosition(position);
                // If user change the default selection
                // First item is disable and it is used for hint
                if (position== 0) {
                    txtLabel.setVisibility(View.GONE);
                    txtRate.setVisibility(View.GONE);
                    txtInclusive.setVisibility(View.GONE);
                    txtCheck.setVisibility(View.GONE);
                    txtUnCheck.setVisibility(View.GONE);
                    editLabel.setVisibility(View.GONE);
                    editRate.setVisibility(View.GONE);
                    taxCheck.setVisibility(View.GONE);
                    // Toast.makeText(getApplicationContext(), "Selected : " + selectedItemText, Toast.LENGTH_SHORT).show();
                }else if(position==1){
                    txtLabel.setVisibility(View.VISIBLE);
                    txtRate.setVisibility(View.VISIBLE);
                    txtInclusive.setVisibility(View.VISIBLE);
                    editLabel.setVisibility(View.VISIBLE);
                    editRate.setVisibility(View.VISIBLE);
                    txtUnCheck.setVisibility(View.VISIBLE);
                    taxCheck.setVisibility(View.VISIBLE);

                }else if(position==2){
                    txtLabel.setVisibility(View.VISIBLE);
                    txtRate.setVisibility(View.VISIBLE);
                    txtInclusive.setVisibility(View.GONE);
                    editLabel.setVisibility(View.VISIBLE);
                    editRate.setVisibility(View.VISIBLE);
                    txtUnCheck.setVisibility(View.GONE);
                    taxCheck.setVisibility(View.GONE);

                }else if(position==3){
                    txtLabel.setVisibility(View.GONE);
                    txtRate.setVisibility(View.GONE);
                    txtInclusive.setVisibility(View.VISIBLE);
                    editLabel.setVisibility(View.GONE);
                    editRate.setVisibility(View.GONE);
                    txtUnCheck.setVisibility(View.VISIBLE);
                    taxCheck.setVisibility(View.VISIBLE);

                }else if (position==4){
                    txtLabel.setVisibility(View.GONE);
                    txtRate.setVisibility(View.GONE);
                    txtInclusive.setVisibility(View.GONE);
                    txtCheck.setVisibility(View.GONE);
                    txtUnCheck.setVisibility(View.GONE);
                    editLabel.setVisibility(View.GONE);
                    editRate.setVisibility(View.GONE);
                    taxCheck.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


    }
}
