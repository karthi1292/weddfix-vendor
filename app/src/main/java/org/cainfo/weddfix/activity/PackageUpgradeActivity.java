package org.cainfo.weddfix.activity;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import org.cainfo.weddfix.R;
import org.cainfo.weddfix.adapter.MtnyViewPagerAdapter;
import org.cainfo.weddfix.fragment.UpgradeDiamondFragment;
import org.cainfo.weddfix.fragment.UpgradeGoldFragment;
import org.cainfo.weddfix.fragment.UpgradeSilverFragment;


public class PackageUpgradeActivity extends AppCompatActivity {
    private TabLayout tabLayout;
    private Toolbar toolbar;
    private ViewPager pager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mtny_upgrade);

        //To support different orientations for mobile and tablet
        if(getApplicationContext().getResources().getBoolean(R.bool.is_tablet)){

            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        }else{

            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }

        tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        pager = (ViewPager) findViewById(R.id.pager_upgrade);
        toolbar = (Toolbar) findViewById(R.id.toolbar);

        toolbar.setTitleTextColor(getResources().getColor(R.color.white));

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();
            }
        });

        setViewPager();
        tabLayout.setupWithViewPager(pager);
    }


    public void setViewPager() {

        MtnyViewPagerAdapter mtnyViewPagerAdapter = new MtnyViewPagerAdapter(getSupportFragmentManager());
        mtnyViewPagerAdapter.addFragment(new UpgradeSilverFragment(), "Silver");
        mtnyViewPagerAdapter.addFragment(new UpgradeGoldFragment(), "Gold");
        mtnyViewPagerAdapter.addFragment(new UpgradeDiamondFragment(), "Diamond");

        pager.setAdapter(mtnyViewPagerAdapter);
    }
}
