package org.cainfo.weddfix.activity;

import android.app.ProgressDialog;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import org.cainfo.weddfix.R;
import org.cainfo.weddfix.adapter.MyListingAdapter;
import org.cainfo.weddfix.model.CustomerModel;

import java.util.ArrayList;

public class MyListingActivity extends AppCompatActivity {
    ProgressDialog progressDialog;
    RecyclerView recyclerView;
    LinearLayoutManager layoutManager;
    ArrayList<CustomerModel> feedList;
    MyListingAdapter adapter_myListing;
    Bundle bundle;
    String[] Names = {"Studio Art", "Balaji Catering"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_listing);

        //To support different orientations for mobile and tablet
        if (getApplicationContext().getResources().getBoolean(R.bool.is_tablet)) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        } else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
        progressDialog = new ProgressDialog(MyListingActivity.this);
        feedList = new ArrayList<>();
        Toolbar toolbar;
        toolbar = (Toolbar) findViewById(R.id.appbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }

        });

        recyclerView = (RecyclerView) findViewById(R.id.recycler_myListing);
        layoutManager = new LinearLayoutManager(MyListingActivity.this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        populatRecyclerView();
    }

    private void populatRecyclerView() {


        for (int i = 0; i < Names.length; i++) {

            feedList.add(new CustomerModel(Names[i], Names[i], Names[i], Names[i]));
        }
        adapter_myListing = new MyListingAdapter(this, feedList);

        recyclerView.setAdapter(adapter_myListing);

        adapter_myListing.notifyDataSetChanged();// Notify the adapter
    }


}
