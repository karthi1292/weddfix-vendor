package org.cainfo.weddfix.activity;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.cainfo.weddfix.Interfaces.VendorRetrofitApiInterface;
import org.cainfo.weddfix.R;
import org.cainfo.weddfix.adapter.VendorCitySpinnerAdapter;
import org.cainfo.weddfix.adapter.VendorCountrySpinnerAdapter;
import org.cainfo.weddfix.adapter.VendorStateSpinnerAdapter;
import org.cainfo.weddfix.apiresponse.MasterRegisterCityDataModel;
import org.cainfo.weddfix.apiresponse.MasterRegisterCountryDataModel;
import org.cainfo.weddfix.apiresponse.MasterRegisterStateDataModel;
import org.cainfo.weddfix.apiresponse.VendorCommonResponseElementModel;
import org.cainfo.weddfix.apiresponse.VendorRegisterCityDataModel;
import org.cainfo.weddfix.apiresponse.VendorRegisterCountryDataModel;
import org.cainfo.weddfix.apiresponse.VendorRegisterStateDataModel;
import org.cainfo.weddfix.common.VendorActivityIndicator;
import org.cainfo.weddfix.common.VendorApiClient;
import org.cainfo.weddfix.common.VendorCommonMethods;
import org.cainfo.weddfix.common.VendorConstants;
import org.cainfo.weddfix.common.VendorSingleton;
import org.cainfo.weddfix.common.VendorStorePreference;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class RegistrationActivity extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemSelectedListener {

    private Button btnSignUp;
    private TextView txtLogin;
    private EditText editName, editEmail, editPwd, editMobile, editPinCode, editConfirmPwd;
    private String uName, uEmail, uPassword, uMobile, uPinCode, uConfirmPwd;
    private VendorRetrofitApiInterface apiService;
    private Context mContext;
    private String cityId, countryId, stateId;
    private AppCompatSpinner citySpinner, countrySpinner, stateSpinner;
    private CheckBox checkBox;
    private VendorActivityIndicator pDialog;
    private VendorStorePreference vendorStorePreference;
    private VendorCitySpinnerAdapter vendorCitySpinnerAdapter;
    private VendorStateSpinnerAdapter vendorStateSpinnerAdapter;
    private VendorCountrySpinnerAdapter vendorCountrySpinnerAdapter;
    private Toolbar toolbar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        //To support different orientations for mobile and tablet
        if (getApplicationContext().getResources().getBoolean(R.bool.is_tablet)) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        } else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }


        mContext = this;
        initWidgets();

        if (VendorCommonMethods.isNetworkConnectionAvailable(this)) {

        } else {
            VendorCommonMethods.showToast(this, VendorConstants.TOAST_CONNECTION_ERROR);
        }

    }


    private void initWidgets() {

        toolbar = (Toolbar) findViewById(R.id.appbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        btnSignUp = (Button) findViewById(R.id.btn_vendor_register);
        txtLogin = (TextView) findViewById(R.id.txt_vendor_login);
        editName = (EditText) findViewById(R.id.ed_vendor_reg_name);
        editEmail = (EditText) findViewById(R.id.ed_vendor_reg_email);
        editMobile = (EditText) findViewById(R.id.ed_vendor_reg_mobile);
        editPinCode = (EditText) findViewById(R.id.ed_vendor_reg_pinCode);
        editPwd = (EditText) findViewById(R.id.ed_vendor_reg_pwd);
        editConfirmPwd = (EditText) findViewById(R.id.ed_vendor_confirm_pwd);
        countrySpinner = (AppCompatSpinner) findViewById(R.id.spinner_vendor_reg_country);
        stateSpinner = (AppCompatSpinner) findViewById(R.id.spinner_vendor_reg_state);
        citySpinner = (AppCompatSpinner) findViewById(R.id.spinner_vendor_reg_city);


        btnSignUp.setOnClickListener(this);
        txtLogin.setOnClickListener(this);
        vendorStorePreference = new VendorStorePreference(mContext);
        apiService = VendorApiClient.getCategoryClient().create(VendorRetrofitApiInterface.class);
    }


    public void sendRegistrationBioData() {

        pDialog = new VendorActivityIndicator(mContext);
        pDialog.setLoadingText("Loading....");
        pDialog.show();

        Call<VendorCommonResponseElementModel> call = apiService.registerVendorData(uName, uEmail, uPassword, uMobile, cityId, stateId, countryId, uPinCode);
        call.enqueue(new Callback<VendorCommonResponseElementModel>() {
            @Override
            public void onResponse(Call<VendorCommonResponseElementModel> call, Response<VendorCommonResponseElementModel> response) {

                if (response.isSuccessful() && response.body() != null) {
                    String status = response.body().vendorCommonResponseModel.status;
                    String message = response.body().vendorCommonResponseModel.message;

                    if (status.equals(VendorConstants.SUCCESS)) {

                        vendorStorePreference.setString(VendorConstants.U_FNAME, uName);
                        vendorStorePreference.setString(VendorConstants.U_EMAIL, uEmail);
                        vendorStorePreference.setString(VendorConstants.U_PINCODE, uPinCode);
                        vendorStorePreference.setString(VendorConstants.U_PHONE, uMobile);
                        vendorStorePreference.setString(VendorConstants.U_STATE, stateId);
                        vendorStorePreference.setString(VendorConstants.U_CITY, cityId);
                        vendorStorePreference.setString(VendorConstants.U_COUNTRY, countryId);


                        Intent intent = new Intent(mContext, LoginActivity.class);
                        startActivity(intent);
                        finish();
                        if (pDialog != null)
                            pDialog.dismiss();
                        Toast.makeText(mContext, message, Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(mContext, message, Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(mContext, VendorConstants.TOAST_CONNECTION_ERROR, Toast.LENGTH_LONG).show();
                    if (pDialog != null)
                        pDialog.dismiss();
                }
            }

            @Override

            public void onFailure(Call<VendorCommonResponseElementModel> call, Throwable t) {
                Log.d("Failure", t.getMessage());
            }
        });
    }
    public void getAllRegisterMasterStateData(String countryId) {

        Call<MasterRegisterStateDataModel> call = apiService.getAllCateStateMasterRegisterData(countryId);
        call.enqueue(new Callback<MasterRegisterStateDataModel>() {
            @Override
            public void onResponse(Call<MasterRegisterStateDataModel> call, Response<MasterRegisterStateDataModel> response) {
                if (response.isSuccessful() && response.body() != null) {
                    MasterRegisterStateDataModel catgMasterStateDataModel = new MasterRegisterStateDataModel();

                    List<VendorRegisterStateDataModel> stateDataModelList = response.body().RegsiterStateDataModelList;

                    VendorRegisterStateDataModel catgRegsiterStateDataModel = new VendorRegisterStateDataModel();
                    catgRegsiterStateDataModel.id = "-1";
                    catgRegsiterStateDataModel.stateName = "State";

                    catgMasterStateDataModel.RegsiterStateDataModelList.add(catgRegsiterStateDataModel);
                    catgMasterStateDataModel.RegsiterStateDataModelList.addAll(stateDataModelList);

                    VendorSingleton.getInstance().catgMasterStateDataModel = catgMasterStateDataModel;

                    vendorStateSpinnerAdapter = new VendorStateSpinnerAdapter(mContext, VendorSingleton.getInstance().catgMasterStateDataModel.RegsiterStateDataModelList);
                    stateSpinner.setAdapter(vendorStateSpinnerAdapter);
                }
            }

            @Override
            public void onFailure(Call<MasterRegisterStateDataModel> call, Throwable t) {
                Log.e("Failure", t.getMessage());
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.btn_vendor_register:

                if (editName.length() > 0 && editEmail.length() > 0 && editMobile.length() > 0 && editPinCode.length() > 0 && editPwd.length() > 0 && editConfirmPwd.length() > 0) {
                    if (countryId != null && stateId != null && cityId != null) {
                        if (!countryId.equals("-1") && !stateId.equals("-1") && !cityId.equals("-1")) {
                            if (editPwd.getText().toString().equals(editConfirmPwd.getText().toString())) {
                                if (editMobile.getText().toString().length() == 10) {
                                    if (VendorCommonMethods.isNetworkConnectionAvailable(mContext)) {

                                        uName = editName.getText().toString();
                                        uEmail = editEmail.getText().toString();
                                        uMobile = editMobile.getText().toString();
                                        uPassword = editPwd.getText().toString();
                                        uPinCode = editPinCode.getText().toString();
                                        uConfirmPwd = editConfirmPwd.getText().toString();

                                        sendRegistrationBioData();
                                    } else {
                                        VendorCommonMethods.showToast(VendorConstants.TOAST_CONNECTION_ERROR, mContext);
                                    }
                                } else {
                                    Toast.makeText(mContext, "Mobile No is Invalid", Toast.LENGTH_LONG).show();
                                }
                            } else {
                                Toast.makeText(mContext, "Password and Confirm Password are not matching", Toast.LENGTH_LONG).show();
                            }
                        } else {
                            Toast.makeText(mContext, "Please fill all the fields", Toast.LENGTH_LONG).show();
                        }
                    } else {
                        Toast.makeText(mContext, "Please fill all the fields", Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(mContext, "Please fill all the fields", Toast.LENGTH_LONG).show();
                }

                break;
            case R.id.txt_vendor_login:
                Intent loginIntent = new Intent(this, LoginActivity.class);
                startActivity(loginIntent);
                break;

        }


    }

    public void getAllMasterRegsiterCityData(String stateId) {

        Call<MasterRegisterCityDataModel> call = apiService.getAllCateCityMasterRegisterData(stateId);
        call.enqueue(new Callback<MasterRegisterCityDataModel>() {
            @Override
            public void onResponse(Call<MasterRegisterCityDataModel> call, Response<MasterRegisterCityDataModel> response) {
                if (response.isSuccessful() && response.body() != null) {

                    MasterRegisterCityDataModel MasterCityDataModel = new MasterRegisterCityDataModel();
                    List<VendorRegisterCityDataModel> cityDataModelList = response.body().vendorRegisterCityDataModels;

                    VendorRegisterCityDataModel RegisterCityDataModel = new VendorRegisterCityDataModel();
                    RegisterCityDataModel.id = "-1";
                    RegisterCityDataModel.cityName = "City";

                    MasterCityDataModel.vendorRegisterCityDataModels.add(RegisterCityDataModel);
                    MasterCityDataModel.vendorRegisterCityDataModels.addAll(cityDataModelList);

                    VendorSingleton.getInstance().catgMasterCityDataModel = MasterCityDataModel;

                    vendorCitySpinnerAdapter = new VendorCitySpinnerAdapter(mContext, VendorSingleton.getInstance().catgMasterCityDataModel.vendorRegisterCityDataModels);
                    citySpinner.setAdapter(vendorCitySpinnerAdapter);
                }
            }

            @Override
            public void onFailure(Call<MasterRegisterCityDataModel> call, Throwable t) {
                Log.e("Failure", t.getMessage());
            }
        });
    }

    public void getAllMasterRegsiterCountryData() {
        Call<MasterRegisterCountryDataModel> call = apiService.getAllCateCountryMasterRegisterData();
        call.enqueue(new Callback<MasterRegisterCountryDataModel>() {
            @Override
            public void onResponse(Call<MasterRegisterCountryDataModel> call, Response<MasterRegisterCountryDataModel> response) {
                if (response.isSuccessful() && response.body() != null) {

                    MasterRegisterCountryDataModel MasterCountryDataModel = new MasterRegisterCountryDataModel();
                    List<VendorRegisterCountryDataModel> countryDataModelList = response.body().categRegisterCountryDataModelList;

                    VendorRegisterCountryDataModel RegisterCountryDataModel = new VendorRegisterCountryDataModel();
                    RegisterCountryDataModel.id = "-1";
                    RegisterCountryDataModel.countryName = "Country";

                    MasterCountryDataModel.categRegisterCountryDataModelList.add(RegisterCountryDataModel);
                    MasterCountryDataModel.categRegisterCountryDataModelList.addAll(countryDataModelList);

                    VendorSingleton.getInstance().catgMasterCountryDataModel = MasterCountryDataModel;
                    if (pDialog != null)
                        pDialog.dismiss();

                    vendorCountrySpinnerAdapter = new VendorCountrySpinnerAdapter(mContext, VendorSingleton.getInstance().catgMasterCountryDataModel.categRegisterCountryDataModelList);
                    countrySpinner.setAdapter(vendorCountrySpinnerAdapter);

                }
            }

            @Override
            public void onFailure(Call<MasterRegisterCountryDataModel> call, Throwable t) {
                Log.e("Failure", t.getMessage());
            }
        });
    }




    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long l) {
        int viewId = parent.getId();
        switch (viewId) {
            case R.id.spinner_vendor_reg_city:
                cityId = vendorCitySpinnerAdapter.getCityId(position);
                break;
            case R.id.spinner_vendor_reg_state:
                stateId = vendorStateSpinnerAdapter.getStateId(position);
                getAllMasterRegsiterCityData(stateId);
                break;
            case R.id.spinner_vendor_reg_country:
                countryId = vendorCountrySpinnerAdapter.getCountryId(position);
                getAllRegisterMasterStateData(countryId);
                break;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

}
