package org.cainfo.weddfix.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import org.cainfo.weddfix.R;
import org.cainfo.weddfix.common.Url;
import org.cainfo.weddfix.common.VendorFontUtil;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;


/**
 * Created by Acer on 27-10-2016.
 */
public class LoginRegister extends AppCompatActivity implements View.OnClickListener {

    Button btn_signIn, btn_register, btn_skip_signin;
    TextView txtWish;
    SharedPreferences sharedPreferences;
    CallbackManager callbackManager;
    LoginButton fbButton;
    String url, url_fb;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FacebookSdk.sdkInitialize(getApplicationContext());
       /* sharedPreferences = PreferenceManager.getDefaultSharedPreferences(LoginRegister.this);

        int logged = sharedPreferences.getInt(Prefs.U_LOGGED, 0);
        if (logged == 1) {
            startActivity(new Intent(LoginRegister.this, HomeActivity.class));
            finish();
        } else {

        }*/


        setContentView(R.layout.login_register);
        //To support different orientations for mobile and tablet
        if (getApplicationContext().getResources().getBoolean(R.bool.is_tablet)) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        } else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
        btn_signIn = (Button) findViewById(R.id.btn_signin);
        btn_register = (Button) findViewById(R.id.btn_register);
        fbButton = (LoginButton) findViewById(R.id.connectWithFbButton);
        btn_register.setOnClickListener(this);
        btn_signIn.setOnClickListener(this);

        Typeface typeface = VendorFontUtil.getHelvetica(LoginRegister.this);

        fbButton.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
        fbButton.setReadPermissions(Arrays.asList("email", "user_photos", "public_profile"));


        callbackManager = CallbackManager.Factory.create();

        fbButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {

                AccessToken accessToken = loginResult.getAccessToken();
                Profile profile = Profile.getCurrentProfile();

                GraphRequest request = GraphRequest.newMeRequest(
                        accessToken,
                        new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(
                                    JSONObject object,
                                    GraphResponse response) {
                                try {

                                    TelephonyManager mngr = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
                                    String ip = mngr.getDeviceId();
                                    url_fb = Url.URL + "" + object.getString("email") + "&name=" + object.getString("name") + "&imei=" + ip + "&device=100";
                                    fb_login();
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,email");
                request.setParameters(parameters);
                request.executeAsync();

                if (profile != null) {

                    // Toast.makeText(Signin.this, profile.getName(), Toast.LENGTH_SHORT).show();

                } else {

                    // Toast.makeText(Signin.this, "profile empty", Toast.LENGTH_SHORT).show();

                }

            }

            @Override
            public void onCancel() {
                // Toast.makeText(Signin.this, "cancel", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError(FacebookException error) {
                //  Toast.makeText(Signin.this, error.toString(), Toast.LENGTH_SHORT).show();

            }
        });

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_signin:
                startActivity(new Intent(LoginRegister.this, LoginActivity.class));

                break;
            case R.id.btn_register:
                startActivity(new Intent(LoginRegister.this, RegistrationActivity.class));
                break;

        }
    }

   /* @Override
    protected void onResume() {
        super.onResume();
        if (sharedPreferences.getInt(Prefs.U_LOGGED, 0) == 0) {

        } else {
            finish();
        }
    }*/
    private void fb_login() {


    }
}
