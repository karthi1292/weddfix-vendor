package org.cainfo.weddfix.activity;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;

import org.cainfo.weddfix.R;

public class UpdateActivity extends AppCompatActivity implements View.OnClickListener {
    Toolbar toolbar;
    private Button btnProfile, btnBusiness, btnAbout, btnBank, btnGallery, btnPackage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_edit);
        //To support different orientations for mobile and tablet
        if(getApplicationContext().getResources().getBoolean(R.bool.is_tablet)){
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        }else{
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
        toolbar = (Toolbar) findViewById(R.id.appbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        btnProfile = (Button) findViewById(R.id.profile_button);
        btnBusiness = (Button) findViewById(R.id.business_button);
        btnAbout = (Button) findViewById(R.id.about_button);
        btnBank = (Button) findViewById(R.id.bank_button);
        btnGallery = (Button) findViewById(R.id.gallary_button);
        btnPackage = (Button) findViewById(R.id.package_button);
        btnProfile.setOnClickListener(this);
        btnBusiness.setOnClickListener(this);
        btnAbout.setOnClickListener(this);
        btnBank.setOnClickListener(this);
        btnGallery.setOnClickListener(this);
        btnPackage.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.profile_button:
                Intent profileIntent = new Intent(this, ProfileUpdateActivity.class);
                startActivity(profileIntent);
                break;
            case R.id.business_button:

                Intent businessIntent = new Intent(this, BusinessUpdateActivity.class);
                startActivity(businessIntent);
                break;
            case R.id.about_button:
                Intent aboutIntent = new Intent(this, AboutBusinessUpdateActivity.class);
                startActivity(aboutIntent);
                break;
            case R.id.bank_button:
                Intent bankIntent = new Intent(this, BankUpdateActivity.class);
                startActivity(bankIntent);
                break;
            case R.id.gallary_button:
                Intent galleryIntent = new Intent(this, GalleryUpdateActivity.class);
                startActivity(galleryIntent);
                break;
            case R.id.package_button:
                Intent packageIntent = new Intent(this,OurPackageUpdateActivity.class);
                startActivity(packageIntent);
                break;

        }
    }
}
