package org.cainfo.weddfix.activity;

import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;


import org.cainfo.weddfix.R;
import org.cainfo.weddfix.adapter.AdapterInboxHome;

import java.util.ArrayList;
import java.util.List;


public class InboxActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    GridLayoutManager layoutManager;
    List<String> inboxlist;
    List<Integer> imglist;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.inbox_home);
        Toolbar toolbar = (Toolbar) findViewById(R.id.appbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        recyclerView = (RecyclerView) findViewById(R.id.recycler_inbox_home);
        int screenSize = getResources().getConfiguration().screenLayout &
                Configuration.SCREENLAYOUT_SIZE_MASK;

        switch (screenSize) {
            case Configuration.SCREENLAYOUT_SIZE_LARGE:
                if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
                    layoutManager = new GridLayoutManager(InboxActivity.this, 4);
                } else {
                    layoutManager = new GridLayoutManager(InboxActivity.this, 3);

                }
                break;
            case Configuration.SCREENLAYOUT_SIZE_NORMAL:
                if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
                    layoutManager = new GridLayoutManager(InboxActivity.this, 3);
                } else {
                    layoutManager = new GridLayoutManager(InboxActivity.this, 2);

                }
                break;
            case Configuration.SCREENLAYOUT_SIZE_SMALL:
                if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
                    layoutManager = new GridLayoutManager(InboxActivity.this, 2);
                } else {
                    layoutManager = new GridLayoutManager(InboxActivity.this, 1);

                }
                break;
            default:
        }
        recyclerView.setLayoutManager(layoutManager);
        inboxlist = new ArrayList<String>();
        imglist = new ArrayList<Integer>();
        imglist.add(R.mipmap.ic_tooltip_edit_black);
        inboxlist.add("COMPOSE");
        imglist.add(R.mipmap.email);
        inboxlist.add("ALL");
        imglist.add(R.mipmap.ic_inbox_black);
        inboxlist.add("INBOX");
        imglist.add(R.mipmap.ic_send_black);
        inboxlist.add("SENT");
        imglist.add(R.mipmap.ic_delete_black);
        inboxlist.add("TRASH");

        recyclerView.setAdapter(new AdapterInboxHome(InboxActivity.this, inboxlist, imglist));
    }
}

