package org.cainfo.weddfix.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.cainfo.weddfix.R;
import org.cainfo.weddfix.activity.InvoiceDescriptionActivity;
import org.cainfo.weddfix.activity.InvoiceDetailActivity;
import org.cainfo.weddfix.activity.InvoiceDiscountActivity;
import org.cainfo.weddfix.activity.InvoiceInfoActivity;
import org.cainfo.weddfix.activity.InvoicePaymentDetailsActivity;
import org.cainfo.weddfix.activity.InvoicePaymentInfoActivity;
import org.cainfo.weddfix.activity.InvoiceTaxActivity;


/**
 * A simple {@link Fragment} subclass.
 */
public class InvoiceEditFragment extends Fragment implements View.OnClickListener {

    TextView invoiceDetails, txtinvoice,txtPaymentDetails;
    CardView cardDescription;
    RelativeLayout relativeLayout,relativeDiscount,relativeTax,relativePaid;

    public InvoiceEditFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_invoice_edit, container, false);
        invoiceDetails = (TextView) view.findViewById(R.id.txt_invoice_businessName);
        txtinvoice = (TextView) view.findViewById(R.id.txt_invoice_date);
        relativeLayout = (RelativeLayout) view.findViewById(R.id.relative1);
        relativeDiscount= (RelativeLayout) view.findViewById(R.id.relative_discount);
        cardDescription = (CardView) view.findViewById(R.id.card_description);
        relativeTax= (RelativeLayout) view.findViewById(R.id.relative_tax);
        relativePaid= (RelativeLayout) view.findViewById(R.id.relative_paid);
        txtPaymentDetails= (TextView) view.findViewById(R.id.txt_invoice_payment_details);


        invoiceDetails.setOnClickListener(this);
        txtinvoice.setOnClickListener(this);
        relativeLayout.setOnClickListener(this);
        relativeTax.setOnClickListener(this);
        cardDescription.setOnClickListener(this);
        relativeDiscount.setOnClickListener(this);
        relativePaid.setOnClickListener(this);
        txtPaymentDetails.setOnClickListener(this);

        return view;


    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.relative1:
                Intent invoiceInfo = new Intent(getActivity(), InvoiceInfoActivity.class);
                startActivity(invoiceInfo);

                break;
            case R.id.card_description:
                Intent invoiceDescription = new Intent(getActivity(), InvoiceDescriptionActivity.class);
                startActivity(invoiceDescription);

                break;
            case R.id.txt_invoice_businessName:
                Intent invoiceDetails = new Intent(getActivity(), InvoiceDetailActivity.class);
                startActivity(invoiceDetails);

                break;
            case R.id.txt_invoice_date:
                Intent invoiceIn = new Intent(getActivity(), InvoiceInfoActivity.class);
                startActivity(invoiceIn);

                break;
            case  R.id.relative_discount:
                Intent invoiceDiscount = new Intent(getActivity(), InvoiceDiscountActivity.class);
                startActivity(invoiceDiscount);

                break;
            case  R.id.relative_tax:

                Intent invoiceTax = new Intent(getActivity(), InvoiceTaxActivity.class);
                startActivity(invoiceTax);
                break;
            case R.id.relative_paid:

                Intent invoicePaid = new Intent(getActivity(), InvoicePaymentInfoActivity.class);
                startActivity(invoicePaid);

                break;
            case R.id.txt_invoice_payment_details:
                Intent invoicePaymentDetails = new Intent(getActivity(), InvoicePaymentDetailsActivity.class);
                startActivity(invoicePaymentDetails);

                break;

        }
    }
}
