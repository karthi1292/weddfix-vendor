package org.cainfo.weddfix.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.bumptech.glide.Glide;

import org.cainfo.weddfix.R;
import org.cainfo.weddfix.activity.MyListingActivity;
import org.cainfo.weddfix.activity.PackageUpgradeActivity;
import org.cainfo.weddfix.activity.ProfileUpdateActivity;
import org.cainfo.weddfix.activity.UpdateActivity;
import org.cainfo.weddfix.activity.UpdateVendorActivity;

/**
 * Created by Acer on 26-10-2016.
 */
public class ListingFragment extends Fragment implements View.OnClickListener {


    TextView txt_pro_name,socialLink;
    ImageView img_profile, listing, addListing, inbox, settings;
    String url_dp_refresh;
    SwipeRefreshLayout swipeRefreshLayout;
    View gview;



    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.myprofile, container, false);
        this.gview = view;

        txt_pro_name = (TextView) view.findViewById(R.id.txt_my_profile_name);
        listing = (ImageView) view.findViewById(R.id.img_listing);
        addListing = (ImageView) view.findViewById(R.id.img_add_listing);
        settings = (ImageView) view.findViewById(R.id.img_settings);
        img_profile = (ImageView) view.findViewById(R.id.img_profile);


        img_profile.setOnClickListener(this);
        listing.setOnClickListener(this);
        addListing.setOnClickListener(this);
        settings.setOnClickListener(this);




        Glide.with(ListingFragment.this).load(R.drawable.list_one).into((ImageView) view.findViewById(R.id.img_listing));
        Glide.with(ListingFragment.this).load(R.drawable.list_one).into((ImageView) view.findViewById(R.id.img_add_listing));
        Glide.with(ListingFragment.this).load(R.drawable.men).into((ImageView) view.findViewById(R.id.img_profile));
        Glide.with(ListingFragment.this).load(R.drawable.price_package).into((ImageView) view.findViewById(R.id.img_settings));


        return view;
    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.img_profile:


                startActivity(new Intent(getActivity().getApplicationContext(), ProfileUpdateActivity.class));


                break;
            case R.id.img_listing:
                startActivity(new Intent(getActivity().getApplicationContext(), MyListingActivity.class));

                break;
            case R.id.img_add_listing:

                startActivity(new Intent(getActivity().getApplicationContext(), UpdateVendorActivity.class));

                break;
            case R.id.img_settings:
                startActivity(new Intent(getActivity().getApplicationContext(), PackageUpgradeActivity.class));
                break;

        }

    }


}
