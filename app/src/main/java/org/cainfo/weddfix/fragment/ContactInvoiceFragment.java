package org.cainfo.weddfix.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.cainfo.weddfix.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class ContactInvoiceFragment extends Fragment {


    public ContactInvoiceFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_contact_invoice, container, false);
    }

}
