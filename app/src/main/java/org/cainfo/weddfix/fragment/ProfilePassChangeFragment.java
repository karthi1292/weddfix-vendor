package org.cainfo.weddfix.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import org.cainfo.weddfix.R;



public class ProfilePassChangeFragment extends Fragment {

    EditText oldpass, newpass, repass;
    Button submit;


    public ProfilePassChangeFragment() {

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.profile_pass_fragment, container, false);


        oldpass = (EditText) view.findViewById(R.id.etoldpass);
        newpass = (EditText) view.findViewById(R.id.etnewpass);
        repass = (EditText) view.findViewById(R.id.etrepass);
        submit = (Button) view.findViewById(R.id.btsave);


        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {




            }
        });


        return view;
    }


}
