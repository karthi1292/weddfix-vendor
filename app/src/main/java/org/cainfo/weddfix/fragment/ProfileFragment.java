package org.cainfo.weddfix.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import org.cainfo.weddfix.R;
import org.cainfo.weddfix.activity.InboxActivity;
import org.cainfo.weddfix.activity.MyListingActivity;
import org.cainfo.weddfix.activity.ProfileUpdateActivity;


/**
 * Created by Acer on 26-10-2016.
 */
public class ProfileFragment extends Fragment implements View.OnClickListener {


    TextView txt_pro_name,socialLink;
    ImageView img_profile, listing, inbox,sociallinks;
    String url_dp_refresh;
    SwipeRefreshLayout swipeRefreshLayout;
    View gview;



    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_myprofile, container, false);
        this.gview = view;

        txt_pro_name = (TextView) view.findViewById(R.id.txt_my_profile_name);
        img_profile = (ImageView) view.findViewById(R.id.img_profile);
        listing = (ImageView) view.findViewById(R.id.img_listing);
        inbox = (ImageView) view.findViewById(R.id.img_inbox);
        sociallinks = (ImageView) view.findViewById(R.id.img_social);
        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swiperefresh_myprofile);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

            }
        });

        img_profile = (ImageView) view.findViewById(R.id.img_profile);
        socialLink = (TextView) view.findViewById(R.id.link_id);
        socialLink.setText("Weddfix-Free android app.Useful for you & your family-http://www.weddfix.com/");
        img_profile.setOnClickListener(this);
        listing.setOnClickListener(this);
        inbox.setOnClickListener(this);
        sociallinks.setOnClickListener(this);




        Glide.with(ProfileFragment.this).load(R.drawable.list_one).into((ImageView) view.findViewById(R.id.img_listing));
       // Glide.with(ProfileFragment.this).load(R.drawable.cart_one).into((ImageView) view.findViewById(R.id.img_order_history));
        Glide.with(ProfileFragment.this).load(R.drawable.chat).into((ImageView) view.findViewById(R.id.img_inbox));
        Glide.with(ProfileFragment.this).load(R.drawable.men).into((ImageView) view.findViewById(R.id.img_profile));
        //Glide.with(ProfileFragment.this).load(R.drawable.setting).into((ImageView) view.findViewById(R.id.img_settings));
        Glide.with(ProfileFragment.this).load(R.drawable.share_one).into((ImageView) view.findViewById(R.id.img_social));


        return view;
    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.img_profile:
                startActivity(new Intent(getActivity().getApplicationContext(), ProfileUpdateActivity.class));
                break;

            case R.id.img_listing:
                startActivity(new Intent(getActivity().getApplicationContext(), MyListingActivity.class));
                break;

           /* case R.id.img_order_history:
                startActivity(new Intent(getActivity().getApplicationContext(), OrderHistory.class));
                break;*/

            case R.id.img_inbox:
                startActivity(new Intent(getActivity().getApplicationContext(), InboxActivity.class));

                break;
            /*case R.id.img_settings:
                startActivity(new Intent(getActivity().getApplicationContext(), ProfileSettings.class));
                break;*/
            case R.id.img_social:
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT,socialLink.getText().toString());
                sendIntent.setType("text/plain");
                Intent.createChooser(sendIntent,"Share via");
                startActivity(sendIntent);

                break;
        }

    }




}
