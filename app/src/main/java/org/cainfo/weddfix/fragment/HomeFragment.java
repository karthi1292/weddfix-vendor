package org.cainfo.weddfix.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.TextView;

import org.cainfo.weddfix.R;
import org.cainfo.weddfix.activity.CustomerActivity;
import org.cainfo.weddfix.activity.WeddOfferActivity;


/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment implements View.OnClickListener {
    private CardView profileView, customerBooking,weddfixOffer;
    TextView quote;
    Fragment fragment;

    public HomeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_home, container, false);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        profileView = (CardView) view.findViewById(R.id.profile_update);
        profileView.setOnClickListener(this);
        customerBooking = (CardView) view.findViewById(R.id.customer_booking);
        weddfixOffer= (CardView) view.findViewById(R.id.card_weddfix_offer);
        weddfixOffer.setOnClickListener(this);
        customerBooking.setOnClickListener(this);

        return view;

    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.profile_update:

                fragment = new ListingFragment();
                FragmentManager fragmentManager1 = getActivity().getSupportFragmentManager();
                fragmentManager1.beginTransaction().replace(R.id.ll_replace_fragment, fragment).commit();
                break;

            case R.id.customer_booking:
                Intent bookingIntent = new Intent(getActivity(), CustomerActivity.class);
                startActivity(bookingIntent);

                break;
            case R.id.card_weddfix_offer:
                Intent offerIntent = new Intent(getActivity(), WeddOfferActivity.class);
                startActivity(offerIntent);

                break;

        }

    }
}
