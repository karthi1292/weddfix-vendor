package org.cainfo.weddfix.apiresponse;

import com.google.gson.annotations.SerializedName;

/**
 * Created by rameshmuthu on 15-02-2017.
 */

public class VendorRegisterCityDataModel {


    @SerializedName("id")
    public String id;
    @SerializedName("cityName")
    public String cityName;

    public VendorRegisterCityDataModel() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }


}
