package org.cainfo.weddfix.apiresponse;

import com.google.gson.annotations.SerializedName;

/**
 * Created by rameshmuthu on 16-02-2017.
 */

public class VendorCommonResponseModel {
    @SerializedName("status")
    public String status;
    @SerializedName("message")
    public String message;


}
