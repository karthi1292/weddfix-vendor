package org.cainfo.weddfix.apiresponse;

import com.google.gson.annotations.SerializedName;

/**
 * Created by rameshmuthu on 15-02-2017.
 */

public class VendorRegisterStateDataModel {
    @SerializedName("id")
    public String id;
    @SerializedName("stateName")
    public String stateName;

    public VendorRegisterStateDataModel() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStateName() {
        return stateName;
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }
}
