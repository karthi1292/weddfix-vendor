package org.cainfo.weddfix.apiresponse;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by rameshmuthu on 15-02-2017.
 */

public class MasterRegisterStateDataModel {


    @SerializedName("state")
    public List<VendorRegisterStateDataModel> RegsiterStateDataModelList=new ArrayList<>();

    public MasterRegisterStateDataModel() {
    }

    public List<VendorRegisterStateDataModel> getRegsiterStateDataModelList() {
        return RegsiterStateDataModelList;
    }

    public void setMasterRegisterStateDataModel(List<VendorRegisterStateDataModel> RegsiterStateDataModelList) {
        this.RegsiterStateDataModelList = RegsiterStateDataModelList;
    }



}
