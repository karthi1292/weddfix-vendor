package org.cainfo.weddfix.apiresponse;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by rameshmuthu on 15-02-2017.
 */

public class MasterRegisterCityDataModel {

    @SerializedName("city")
    public List<VendorRegisterCityDataModel> vendorRegisterCityDataModels=new ArrayList<>();

    public MasterRegisterCityDataModel() {
    }

    public List<VendorRegisterCityDataModel> getcategRegisterCityDataModelList() {
        return vendorRegisterCityDataModels;
    }

    public void setMtnyRegisterCityDataModelList(List<VendorRegisterCityDataModel> categRegisterCityDataModelList) {
        this.vendorRegisterCityDataModels = categRegisterCityDataModelList;
    }

}
