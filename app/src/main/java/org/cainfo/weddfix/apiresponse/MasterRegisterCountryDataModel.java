package org.cainfo.weddfix.apiresponse;

import com.google.gson.annotations.SerializedName;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by rameshmuthu on 15-02-2017.
 */

public class MasterRegisterCountryDataModel {

    @SerializedName("country")
    public List<VendorRegisterCountryDataModel> categRegisterCountryDataModelList=new ArrayList<>();

    public MasterRegisterCountryDataModel() {
    }

    public List<VendorRegisterCountryDataModel> getcategRegisterCityDataModelList() {
        return categRegisterCountryDataModelList;
    }

    public void setCategRegisterCountryDataModelList(List<VendorRegisterCountryDataModel> categRegisterCityDataModelList) {
        this.categRegisterCountryDataModelList = categRegisterCityDataModelList;
    }



}
