package org.cainfo.weddfix.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import org.cainfo.weddfix.R;
import org.cainfo.weddfix.activity.PackageUpgradeActivity;
import org.cainfo.weddfix.model.CustomerModel;

import java.util.List;

/**
 * Created by rameshmuthu on 06-02-2017.
 */

public class MyListingAdapter extends RecyclerView.Adapter<MyListingAdapter.ViewHolder> {

    Context context;
    List<CustomerModel> nameList;


    public MyListingAdapter(Context context, List<CustomerModel> name) {
        this.context = context;
        this.nameList = name;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(context).inflate(R.layout.custome_my_listing_row, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        CustomerModel feedback = nameList.get(position);

        MyListingAdapter.ViewHolder mainHolder = (MyListingAdapter.ViewHolder) holder;// holder

        mainHolder.cName.setText(feedback.getName());
        mainHolder.  upgradeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent customer= new Intent(context, PackageUpgradeActivity.class);
                context.startActivity(customer);
            }
        });


    }


    @Override
    public int getItemCount() {

        return nameList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView cName, cRemove, cDelete;
        ImageView img_customer;
        Button upgradeBtn;


        public ViewHolder(View itemView) {
            super(itemView);
            cName = (TextView) itemView.findViewById(R.id.txt_custom_myLising_cName);
            img_customer = (ImageView) itemView.findViewById(R.id.img_custom_myListing);
            cRemove = (TextView) itemView.findViewById(R.id.txt_custom_myListing_cDelete);
            upgradeBtn = (Button) itemView.findViewById(R.id.btn_upgrade_public);
            cDelete = (TextView) itemView.findViewById(R.id.txt_custom_myListing_cEdit);

        }

    }


}
