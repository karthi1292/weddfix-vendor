package org.cainfo.weddfix.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;


import org.cainfo.weddfix.R;
import org.cainfo.weddfix.apiresponse.VendorRegisterCountryDataModel;

import java.util.List;

/**
 * Created by rameshmuthu on 15-02-2017.
 */

public class VendorCountrySpinnerAdapter extends BaseAdapter {

    private Context mContext;
    private List<VendorRegisterCountryDataModel> catgCountryDataModelList;

    public VendorCountrySpinnerAdapter(Context context, List<VendorRegisterCountryDataModel> countryDataModelList) {
        this.mContext=context;
        this.catgCountryDataModelList=countryDataModelList;
    }

    @Override
    public int getCount() {
        return catgCountryDataModelList.size();
    }

    @Override
    public VendorRegisterCountryDataModel getItem(int position) {
        return catgCountryDataModelList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }
    public String getCountryName(int position){
        return getItem(position).getCountryName();
    }
    public String getCountryId(int position){
        return getItem(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        VendorCountrySpinnerAdapter.ViewHolder holder;
        LayoutInflater inflater=(LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if(convertView==null){
            holder=new VendorCountrySpinnerAdapter.ViewHolder();
            convertView=inflater.inflate(R.layout.common_spinner,parent,false);
            holder.txtCountryName=(TextView)convertView.findViewById(R.id.state_name);
            convertView.setTag(holder);
        }
        else{
            holder=(VendorCountrySpinnerAdapter.ViewHolder)convertView.getTag();
        }
        holder.txtCountryName.setText(getCountryName(position));
        return convertView;
    }

    private class ViewHolder{
        private TextView txtCountryName;
    }
}
