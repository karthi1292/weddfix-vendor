package org.cainfo.weddfix.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import org.cainfo.weddfix.R;
import org.cainfo.weddfix.activity.EditCustomerActivity;

import java.util.List;

/**
 * Created by rameshmuthu on 16-12-2016.
 */
public class AddCustomerAdapter extends RecyclerView.Adapter<AddCustomerAdapter.ViewHolder>{
   // private final View.OnClickListener mOnClickListener = new MyOnClickListener();

    List<String>client_id, client_name,client_email,client_city, client_address,client_state,client_zip;
    Context context;

    public AddCustomerAdapter(Context context, List<String> client_id, List<String> client_name, List<String> client_email, List<String> client_city , List<String> client_address, List<String> client_state, List<String> client_zip) {
        this.context = context;
        this.client_id = client_id;
        this.client_name = client_name;
        this.client_email = client_email;
        this.client_city = client_city;
        this.client_address = client_address;
        this.client_state = client_state;
        this.client_zip = client_zip;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(context).inflate(R.layout.custome_add_customer, parent, false);
        return new ViewHolder(view);

    }


    public void onBindViewHolder(final ViewHolder holder, final int position) {

        holder.client_name.setText(client_name.get(position));
        holder.client_add.setText(client_address.get(position));
        holder.client_cit.setText(client_city.get(position));
        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(context, EditCustomerActivity.class);
                i.putExtra("id", client_id.get(position));
                i.putExtra("name", client_name.get(position));
                i.putExtra("email", client_email.get(position));
                i.putExtra("city", client_city.get(position));
                i.putExtra("address", client_address.get(position));
                i.putExtra("state", client_state.get(position));
                i.putExtra("zip", client_zip.get(position));
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                notifyDataSetChanged();
                context.startActivity(i);

            }
        });

    }

    @Override
    public int getItemCount() {
        return client_name.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView client_name, client_add,client_cit;
        CardView cardView;

        public ViewHolder(View itemView) {
            super(itemView);

            client_name = (TextView) itemView.findViewById(R.id.customer_name);
            client_add = (TextView) itemView.findViewById(R.id.client_address);
            client_cit = (TextView) itemView.findViewById(R.id.client_city);
            cardView = (CardView) itemView.findViewById(R.id.orderlist_card_view);
        }
    }
}
