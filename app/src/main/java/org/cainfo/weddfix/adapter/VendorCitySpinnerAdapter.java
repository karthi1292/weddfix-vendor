package org.cainfo.weddfix.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;


import org.cainfo.weddfix.R;
import org.cainfo.weddfix.apiresponse.VendorRegisterCityDataModel;

import java.util.List;

/**
 * Created by rameshmuthu on 16-03-2017.
 */

public class VendorCitySpinnerAdapter extends BaseAdapter {

    private Context mContext;
    private List<VendorRegisterCityDataModel> vendorRegisterCityDataModels;


    public VendorCitySpinnerAdapter(Context context, List<VendorRegisterCityDataModel> catgRegisterCityDataModelList) {
        this.mContext=context;
        this.vendorRegisterCityDataModels=catgRegisterCityDataModelList;
    }
    @Override
    public int getCount() {
        return vendorRegisterCityDataModels.size();
    }

    @Override
    public VendorRegisterCityDataModel getItem(int position) {
        return vendorRegisterCityDataModels.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }
    public String getCityName(int position) {
        return getItem(position).getCityName();
    }
    public String getCityId(int position) {
        return getItem(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {
        VendorCitySpinnerAdapter.ViewHolder holder;
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            holder = new VendorCitySpinnerAdapter.ViewHolder();
            convertView = inflater.inflate(R.layout.common_spinner, viewGroup, false);
            holder.txtCityName = (TextView) convertView.findViewById(R.id.state_name);
            convertView.setTag(holder);
        } else {
            holder = (VendorCitySpinnerAdapter.ViewHolder) convertView.getTag();
        }
        holder.txtCityName.setText(getCityName(position));
        return convertView;
    }

    private class ViewHolder {
        private TextView txtCityName;
    }
}
