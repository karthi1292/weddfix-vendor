package org.cainfo.weddfix.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import org.cainfo.weddfix.R;

import java.util.List;


/**
 * Created by rameshmuthu on 28-12-2016.
 */
public class AdapterGalleryUpdate extends RecyclerView.Adapter<AdapterGalleryUpdate.ViewHolder> {

    Context context;
    List<Bitmap> bitmapList;

    public AdapterGalleryUpdate(Context context, List<Bitmap> bitmapList) {
        this.context = context;
        this.bitmapList = bitmapList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(context).inflate(R.layout.custom_gallery_update, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        holder.floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        //Glide.with(context).load(bitmapList.get(position)).into(holder.imageView);
        holder.imageView.setImageBitmap(bitmapList.get(position));

    }

    @Override
    public int getItemCount() {
        return bitmapList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        FloatingActionButton floatingActionButton;
        ImageView imageView;

        public ViewHolder(View itemView) {
            super(itemView);

            floatingActionButton = (FloatingActionButton) itemView.findViewById(R.id.fab_custom_post_photo_fragment);
            imageView = (ImageView) itemView.findViewById(R.id.img_custom_post_photo_fragment);

        }
    }
}
