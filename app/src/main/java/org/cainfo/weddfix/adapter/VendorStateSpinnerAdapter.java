package org.cainfo.weddfix.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;


import org.cainfo.weddfix.R;
import org.cainfo.weddfix.apiresponse.VendorRegisterStateDataModel;

import java.util.List;

/**
 * Created by rameshmuthu on 15-02-2017.
 */

public class VendorStateSpinnerAdapter extends BaseAdapter {

    private Context mContext;
    private List<VendorRegisterStateDataModel> stateDataModelArrayList;

    public VendorStateSpinnerAdapter(Context context, List<VendorRegisterStateDataModel> catgRegisterStateDataModelList) {
        this.mContext=context;
        this.stateDataModelArrayList=catgRegisterStateDataModelList;
    }
    @Override
    public int getCount() {
        return stateDataModelArrayList.size();
    }

    @Override
    public VendorRegisterStateDataModel getItem(int position) {
        return stateDataModelArrayList.get(position);
    }


    @Override
    public long getItemId(int position) {
        return position;
    }
    public String getStateName(int position) {
        return getItem(position).getStateName();
    }
    public String getStateId(int position) {
        return getItem(position).getId();
    }
    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {
        VendorStateSpinnerAdapter.ViewHolder holder;
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            holder = new VendorStateSpinnerAdapter.ViewHolder();
            convertView = inflater.inflate(R.layout.common_spinner, viewGroup, false);
            holder.txtStateName = (TextView) convertView.findViewById(R.id.state_name);
            convertView.setTag(holder);
        } else {
            holder = (VendorStateSpinnerAdapter.ViewHolder) convertView.getTag();
        }
        holder.txtStateName.setText(getStateName(position));
        return convertView;
    }

    private class ViewHolder {
        private TextView txtStateName;
    }
}
