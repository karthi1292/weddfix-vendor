package org.cainfo.weddfix.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


import org.cainfo.weddfix.R;
import org.cainfo.weddfix.activity.CustomerDetailsActivity;
import org.cainfo.weddfix.model.CustomerModel;

import java.util.List;


/**
 * Created by Acer on 01-07-2016.
 */
public class AdapterCustomer extends RecyclerView.Adapter<AdapterCustomer.ViewHolder> {

    Context context;
    List<CustomerModel> nameList;

    public AdapterCustomer(Context context, List<CustomerModel> name) {
        this.context = context;
        this.nameList = name;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(context).inflate(R.layout.custome_add_customer, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        //Glide.with(context).load(img_list.get(position)).into(holder.imageView);

        CustomerModel feedback = nameList.get(position);

        ViewHolder mainHolder = (ViewHolder) holder;// holder

        mainHolder.feedback_name.setText(feedback.getName());
        mainHolder.customerList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent customer= new Intent(context, CustomerDetailsActivity.class);
                context.startActivity(customer);
            }
        });


    }

    @Override
    public int getItemCount() {
        return nameList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView feedback_name;
        CardView customerList;


        public ViewHolder(View itemView) {
            super(itemView);

            feedback_name = (TextView) itemView.findViewById(R.id.customer_name);
            customerList= (CardView) itemView.findViewById(R.id.orderlist_card_view);
        }
    }
}
