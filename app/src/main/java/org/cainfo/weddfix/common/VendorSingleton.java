package org.cainfo.weddfix.common;


import org.cainfo.weddfix.apiresponse.MasterRegisterCityDataModel;
import org.cainfo.weddfix.apiresponse.MasterRegisterCountryDataModel;
import org.cainfo.weddfix.apiresponse.MasterRegisterStateDataModel;

/**
 * Created by rameshmuthu on 16-03-2017.
 */

public class VendorSingleton {

    private static VendorSingleton instance = null;

    public static VendorSingleton getInstance() {
        if (instance == null) {
            synchronized (VendorSingleton.class) {
                if (instance == null) {
                    instance = new VendorSingleton();
                }
            }
        }
        return instance;
    }


    public MasterRegisterCityDataModel catgMasterCityDataModel = new MasterRegisterCityDataModel();
    public MasterRegisterStateDataModel catgMasterStateDataModel = new MasterRegisterStateDataModel();
    public MasterRegisterCountryDataModel catgMasterCountryDataModel = new MasterRegisterCountryDataModel();




}
