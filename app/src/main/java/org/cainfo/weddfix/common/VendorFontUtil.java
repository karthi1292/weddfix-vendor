package org.cainfo.weddfix.common;

import android.content.Context;
import android.graphics.Typeface;

/**
 *Created by rameshmuthu on 16-03-2017.
 */
public class VendorFontUtil {
    public static Typeface typeface;
    public static String helveticaFontPath = "fonts/Helvetica.ttf";

    //to get Helvetica Regular font typeface
    public static Typeface getHelvetica(Context mContext){
        return Typeface.createFromAsset(mContext.getAssets(), helveticaFontPath);
    }

}
