package org.cainfo.weddfix.common;


public class Config {
    // File upload url (replace the ip with your server address)
    public static final String FILE_UPLOAD_URL = Url.URL + "";

    // Directory name to store captured images and videos
    public static final String IMAGE_DIRECTORY_NAME = "Weddfix";
}
