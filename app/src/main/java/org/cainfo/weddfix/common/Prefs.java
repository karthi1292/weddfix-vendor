package org.cainfo.weddfix.common;

/**
 * Created by rameshmuthu on 28-12-2016.
 */
public class Prefs {

    public static String U_FNAME = "user_fname";
    public static String U_LNAME = "user_lname";
    public static String U_GENDER = "user_gender";
    public static String U_DOB = "user_dob";
    public static String U_NAME = "user_name";
    public static String U_ID = "user_id";
    public static String U_EMAIL = "user_email";
    public static String U_PHONE = "user_phone";
    public static String U_ADDRESS = "user_address";
    public static String U_STATUS = "user_status";
    public static String U_PRO_PIC = "user_dp";
    public static String U_EMAIL_ACTIVE = "user_email_active";
    public static String U_LOGGED = "user_logged";

}
