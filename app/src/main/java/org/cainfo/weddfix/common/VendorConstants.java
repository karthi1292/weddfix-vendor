package org.cainfo.weddfix.common;

import android.Manifest;

/**
 * Created by rameshmuthu on 16-03-2017.
 */

public class VendorConstants {
     public static String SUCCESS="success";
     public static String TOAST_CONNECTION_ERROR="Communication Failure";
     public static final String MyPREFERENCESNAME = "WeddfixCategory";
     public static String[] PERMISSIONS = {
             Manifest.permission.READ_CONTACTS, Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.WRITE_EXTERNAL_STORAGE
     };

     // File upload url
     public static final String FILE_UPLOAD_URL = VendorApiClient.CATEGORY_BASE_URL + "";

     // Directory name to store captured images and videos
     public static final String IMAGE_DIRECTORY_NAME = "Weddfix";


     //CategoryStorePreference MtnyConstants
     public static String U_FNAME = "userName";
     public static String U_GENDER = "user_gender";
     public static String U_WEDDING_DATE = "weddingDate";
     public static String U_NAME = "user_name";
     public static String USER_ID = "userId";
     public static String U_PINCODE="pincode";
     public static String U_EMAIL = "email";
     public static String U_PHONE = "mobile";
     public static String U_STATE = "stateId";
     public static String U_CITY = "cityId";
     public static String U_COUNTRY = "countryId";
     public static String U_ADDRESS = "user_address";
     public static String U_STATUS = "userStatus";
     public static String U_PRO_PIC = "fileName";
     public static String U_EMAIL_ACTIVE = "user_email_active";
     public static String U_LOGGED = "loggedIn";


     //CategoryNames
     public static String WEDDING_HALL = "wedding_hall";
     public static String STUDIOS = "studios";
     public static String DECORATIONS = "decorations";
     public static String BEAUTY_PARLOUR = "beauty_parlour";
     public static String JEWEL_SHOPS = "jewel_shops";
     public static String CATERINGS = "caterings";
     public static String ENTERTAINMENT = "entertainment";
     public static String WED_CARDS = "wed_cards";
     public static String TEXTILES = "textiles";
     public static String TRAVELS = "travels";
     public static String H0TELS = "hotels";
     public static String ASTROLOGERS = "astrologers";

}
