package org.cainfo.weddfix.Interfaces;

import android.app.Dialog;
import android.widget.EditText;


public abstract interface DialogResponse {
    void onOkButtonClicked(Dialog builder, EditText alert);
    void onCancleButtonClicked(Dialog builder);


}
