package org.cainfo.weddfix.Interfaces;

import org.cainfo.weddfix.apiresponse.MasterRegisterCityDataModel;
import org.cainfo.weddfix.apiresponse.MasterRegisterCountryDataModel;
import org.cainfo.weddfix.apiresponse.MasterRegisterStateDataModel;
import org.cainfo.weddfix.apiresponse.VendorCommonResponseElementModel;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by rameshmuthu on 16-03-2017.
 */

public interface VendorRetrofitApiInterface {

    @GET("vendorregister.json")
    Call<MasterRegisterCityDataModel> getAllCateCityMasterRegisterData(@Query("state_Id") String stateId);

    @FormUrlEncoded
    @POST("vendorregister.json?save=register")
    Call<VendorCommonResponseElementModel> registerVendorData(@Field("fullName") String fullName, @Field("email") String email, @Field("password") String password, @Field("mobile") String mobile, @Field("cityId") String cityId, @Field("stateId") String stateId, @Field("countryId") String countryId, @Field("pincode") String pincode);


    @GET("vendorregister.json")
    Call<MasterRegisterStateDataModel> getAllCateStateMasterRegisterData(@Query("country_Id") String countryId);


    @GET("vendorregister.json")
    Call<MasterRegisterCountryDataModel> getAllCateCountryMasterRegisterData();
}
